<?php

use App\Enums\Language;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('isShownToUser')->default(false);
            $table->string('password');
            $table->boolean('isConfirmed')->default(false);
            $table->boolean('isBlocked')->default(false);
            $table->string('language')->default(Language::LV);
            $table->boolean('showInactive')->default(true);
            $table->boolean('showDeleted')->default(true);
            $table->rememberToken();
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
            $table->timestamp('passEditedAt')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
