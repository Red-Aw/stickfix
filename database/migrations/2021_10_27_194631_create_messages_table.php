<?php

use App\Enums\Language;
use App\Enums\MessageType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->enum('option', MessageType::getValues());
            $table->string('name', 256);
            $table->string('email', 256);
            $table->string('phoneNumber', 32);
            $table->string('phoneCountry', 5);
            $table->text('description')->default('')->nullable();
            $table->string('clientLanguage')->default(Language::LV);
            $table->boolean('isRead')->default(false);
            $table->boolean('isAnswered')->default(false);
            $table->boolean('isSentToExternalMail')->default(false);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
