<?php

use App\Enums\BrandType;
use App\Enums\CategoryType;
use App\Enums\ConditionType;
use App\Enums\GenderType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->integer('userId')->unsigned();
            $table->string('title', 256);
            $table->string('slug', 256);
            $table->text('description')->default('')->nullable();
            $table->integer('priceInMinorUnit')->nullable();
            $table->enum('category', CategoryType::getValues());
            $table->boolean('isActive')->default(true);
            $table->boolean('isDeleted')->default(false);
            $table->enum('state', ConditionType::getValues());
            $table->enum('brand', BrandType::getValues())->nullable();
            $table->string('size', 32)->nullable();
            $table->string('stickSize', 32)->nullable();
            $table->string('stickFlex', 32)->nullable();
            $table->string('bladeCurve', 32)->nullable();
            $table->string('bladeSide', 32)->nullable();
            $table->string('skateSize', 32)->nullable();
            $table->string('skateLength', 32)->nullable();
            $table->string('skateWidth', 32)->nullable();
            $table->boolean('isAvailableForSale')->default(true);
            $table->integer('quantity');
            $table->enum('gender', GenderType::getValues())->nullable();
            $table->boolean('isParcelDelivery')->default(true);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
