<?php

use App\Enums\DeliveryType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shipping_data', function (Blueprint $table) {
            $table->id();
            $table->enum('deliveryType', DeliveryType::getValues())->unique();
            $table->integer('deliveryPriceInMinorUnit')->default(500);
            $table->integer('freeDeliveryStartingPriceInMinorUnit')->default(10000);
            $table->boolean('isActive')->default(1);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shipping_data');
    }
};
