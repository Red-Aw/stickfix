<?php

use App\Enums\CountryCode;
use App\Enums\DeliveryType;
use App\Enums\Language;
use App\Enums\OrderStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('stripeSessionId', 512)->unique();
            $table->enum('status', OrderStatus::getValues());
            $table->json('productList');
            $table->integer('productAmountInMinorUnit');
            $table->integer('shippingAmountInMinorUnit');
            $table->integer('totalAmountInMinorUnit');
            $table->string('clientFirstName', 256);
            $table->string('clientLastName', 256);
            $table->string('clientEmail', 256);
            $table->string('clientPhoneCountry', 5);
            $table->string('clientPhoneNumber', 32);
            $table->string('clientLanguageCode')->default(Language::LV);
            $table->enum('deliveryType', DeliveryType::getValues());
            $table->string('parcelMachineName', 256);
            $table->enum('clientCountryCode', CountryCode::getValues())->default(CountryCode::LV);
            $table->string('clientState', 256)->nullable();
            $table->string('clientCity', 256);
            $table->string('clientAddress', 256);
            $table->string('clientFullAddress', 256);
            $table->string('clientZipCode', 32);
            $table->boolean('isShownToClient')->default(false);
            $table->boolean('isNotificationSent')->default(false);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('processedAt')->nullable();
            $table->timestamp('sentOutAt')->nullable();
            $table->timestamp('finishedAt')->nullable();
            $table->timestamp('lastOpenedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
