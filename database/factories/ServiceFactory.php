<?php

namespace Database\Factories;

use App\Enums\ServiceType;
use App\Models\Service;
use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory<Service> */
class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<\App\Models\Service>
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'serviceType' => ServiceType::OFFER,
            'title' => 'Two exquisite objection delighted',
            'note' => 'Two exquisite objection delighted deficient yet its contained. Cordial because are account evident its subject but eat.',
            'language' => null,
            'priceInMinorUnit' => 800,
            'isActive' => 1,
            'createdAt' => now(),
            'editedAt' => now(),
        ];
    }
}
