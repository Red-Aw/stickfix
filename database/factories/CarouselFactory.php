<?php

namespace Database\Factories;

use App\Models\Carousel;
use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory<Carousel> */
class CarouselFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<\App\Models\Carousel>
     */
    protected $model = Carousel::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => 'day giving points within six not law',
            'imageName' => 'thzvhkgy.png',
            'imagePath' => 'after/something',
            'language' => null,
            'isActive' => 1,
            'createdAt' => now(),
            'editedAt' => now(),
        ];
    }
}
