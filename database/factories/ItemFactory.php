<?php

namespace Database\Factories;

use App\Enums\BrandType;
use App\Enums\CategoryType;
use App\Enums\ConditionType;
use App\Enums\SkateLengthSize;
use App\Enums\SkateSize;
use App\Enums\SkateWidthSize;
use App\Models\Item;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory<Item> */
class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<\App\Models\Item>
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'userId' => User::factory(),
            'title' => 'Perpetual',
            'description' => 'Perpetual sincerity out suspected necessary one but provision satisfied. Respect nothing use set waiting pursuit nay you looking. If on prevailed concluded ye abilities. Address say you new but minuter greater. Do denied agreed in innate. Can and middletons thoroughly themselves him. Tolerably sportsmen belonging in september no am immediate newspaper. Theirs expect dinner it pretty indeed having no of. Principle september she conveying did eat may extensive.',
            'priceInMinorUnit' => 10000,
            'category' => CategoryType::SKATES,
            'isActive' => true,
            'isDeleted' => false,
            'state' => ConditionType::NEW,
            'brand' => BrandType::NONE,
            'size' => null,
            'stickSize' => null,
            'stickFlex' => null,
            'bladeCurve' => null,
            'bladeSide' => null,
            'skateSize' => SkateSize::INT,
            'skateLength' => SkateLengthSize::INT_4_0,
            'skateWidth' => SkateWidthSize::C,
            'isAvailableForSale' => true,
            'quantity' => 1,
            'gender' => null,
            'isParcelDelivery' => true,
            'createdAt' => now(),
            'editedAt' => now(),
        ];
    }
}
