<?php

namespace App\Models;

use App\Models\Item;
use App\Models\Role;
use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Summary of User
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'isShownToUser',
        'isConfirmed',
        'isBlocked',
        'language',
        'showInactive',
        'showDeleted',
        'createdAt',
        'editedAt',
        'passEditedAt',
    ];

    /**
     * Summary of hidden
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Summary of casts
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Summary of items
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Item>
     */
    public function items(): HasMany
    {
        return $this->hasMany(Item::class, 'userId');
    }

    /**
     * Summary of roles
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Role>
     */
    public function roles(): HasMany
    {
        return $this->hasMany(Role::class, 'userId');
    }

    /**
     * Summary of sendEmailVerificationNotification
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        if (config('app.env') === 'production') {
            $this->notify(new VerifyEmail);
        }
    }
}
