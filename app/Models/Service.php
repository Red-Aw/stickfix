<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Summary of Service
 */
class Service extends Model
{
    use HasFactory;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'serviceType',
        'title',
        'note',
        'language',
        'priceInMinorUnit',
        'isActive',
        'createdAt',
        'editedAt',
    ];
}
