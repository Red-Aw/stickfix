<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Summary of ShippingData
 */
class ShippingData extends Model
{
    use HasFactory;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'deliveryType',
        'deliveryPriceInMinorUnit',
        'freeDeliveryStartingPriceInMinorUnit',
        'isActive',
        'createdAt',
        'editedAt',
    ];
}
