<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Summary of Image
 */
class Image extends Model
{
    use HasFactory;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'itemId',
        'offerId',
        'name',
        'path',
        'createdAt',
        'editedAt',
    ];

    /**
     * Summary of item
     * @return void
     */
    public function item()
    {
        $this->belongsTo(Item::class, 'id');
    }
}
