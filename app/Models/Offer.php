<?php

namespace App\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Summary of Offer
 */
class Offer extends Model
{
    use HasFactory, HasRelationships;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'description',
        'language',
        'isActive',
        'createdAt',
        'editedAt',
    ];

    /**
     * Summary of images
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Image>
     */
    public function images(): HasMany
    {
        return $this->hasMany(Image::class, 'offerId');
    }
}
