<?php

namespace App\Http\Policies;

use Spatie\Csp\Directive;
use Spatie\Csp\Keyword;
use Spatie\Csp\Policies\Policy;
use Spatie\Csp\Scheme;

/**
 * Summary of Extended
 */
class Extended extends Policy
{
	/**
	 * Summary of configure
	 * @return void
	 */
    public function configure(): void
    {
        if (config('app.env') !== 'production') {
			$this->addDirective(
				Directive::CONNECT,
				Scheme::WS
			);
			$this->addDirective(
				Directive::CONNECT,
				config('app.vite_server_url')
			);
            $this->addDirective(
				Directive::DEFAULT,
				config('app.vite_server_url')
			);
            $this->addDirective(
				Directive::SCRIPT,
				config('app.vite_server_url')
			);
            $this->addDirective(
				Directive::STYLE,
				config('app.vite_server_url')
			);
            $this->addDirective(
				Directive::IMG,
				config('app.vite_server_url')
			);
		}

        $this
			->addDirective(Directive::BASE, Keyword::SELF)
			->addDirective(Directive::CONNECT, Keyword::SELF)
			->addDirective(Directive::DEFAULT, Keyword::SELF)
			->addDirective(Directive::FORM_ACTION, Keyword::SELF)
			->addDirective(Directive::IMG, Keyword::SELF)
			->addDirective(Directive::MEDIA, Keyword::SELF)
			->addDirective(Directive::OBJECT, Keyword::NONE)
			->addDirective(Directive::SCRIPT, Keyword::SELF)
			->addDirective(Directive::STYLE, Keyword::SELF);

		$this->addDirective(Directive::CONNECT, [
			'www.omniva.lv',
		]);
		$this->addDirective(Directive::SCRIPT, [
			Keyword::UNSAFE_EVAL,
			Keyword::UNSAFE_INLINE,
			'maps.googleapis.com',
			'www.gstatic.com',
			'www.google.com',
			'checkout.stripe.com',
			'recaptcha.net',
		]);
		$this->addDirective(Directive::STYLE, [
			Keyword::UNSAFE_INLINE,
			'fonts.googleapis.com',
		]);
		$this->addDirective(Directive::DEFAULT, [
			'fonts.gstatic.com',
			'www.google.com',
			'recaptcha.net',
		]);
		$this->addDirective(Directive::IMG, [
			Keyword::UNSAFE_INLINE,
			'data:',
			'blob:',
		]);
    }
}