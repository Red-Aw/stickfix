<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

/**
 * Summary of EncryptCookies
 */
class EncryptCookies extends Middleware
{

    /**
     * Summary of except
     * @var array<int, string>
     */
    protected $except = [
        //
    ];
}
