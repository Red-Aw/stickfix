<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance as Middleware;

/**
 * Summary of PreventRequestsDuringMaintenance
 */
class PreventRequestsDuringMaintenance extends Middleware
{
    /**
     * Summary of except
     * @var array<int, string>
     */
    protected $except = [
        //
    ];
}
