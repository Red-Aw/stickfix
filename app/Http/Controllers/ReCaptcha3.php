<?php

namespace App\Http\Controllers;

/**
 * Summary of ReCaptcha3
 */
class ReCaptcha3 extends Controller
{
    const URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * Summary of verify
     * @param string $token
     * @return bool
     */
    public static function verify(string $token): bool
    {
        if (config('app.env') === 'testing') {
            return true;
        }

        $params = [
            'secret' => config('services.recaptcha.secret'),
            'response' => $token,
        ];

        $options = [
            'http' => [
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'method'  => 'POST',
                'content' => http_build_query($params)
            ]
        ];

        $context  = stream_context_create($options);
        $result = file_get_contents(static::URL, false, $context);
        if (!$result) {
            return false;
        }
        $resultJson = json_decode($result);

        return $resultJson->success ? true : false;
    }
}
