<?php

namespace App\Http\Controllers;

use App\Enums\MailerType;
use App\Enums\MessageType;
use App\Http\Controllers\ReCaptcha3;
use App\Mail\NewMail;
use App\Mail\NewReply;
use App\Models\Message;
use App\Models\Reply;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of MessageController
 */
class MessageController extends Controller
{
    const MESSAGE_OPTIONS = array(
        MessageType::APPLY_FOR_REPAIR => array(
            'lv' => 'Pieteikt remontu',
            'en' => 'Apply for repair',
            'ru' => 'Подать заявку на ремонт',
        ),
        MessageType::APPLY_FOR_ITEM => array(
            'lv' => 'Pieteikt preci',
            'en' => 'Apply for the product',
            'ru' => 'Подать заявку на продукт',
        ),
        MessageType::QUESTION => array(
            'lv' => 'Jautājums',
            'en' => 'Question',
            'ru' => 'Вопрос',
        ),
        MessageType::OTHER => array(
            'lv' => 'Cits',
            'en' => 'Other',
            'ru' => 'Другой',
        ),
    );

    /**
     * Summary of getMessages
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessages()
    {
        try {
            $messages = Message::orderBy('createdAt', 'desc')->get();
            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $messages = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'messages' => $messages,
        ]);
    }

    /**
     * Summary of getMessage
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessage(int $id)
    {
        try {
            /** @var Message $messageData */
            $messageData = Message::with('replies')->findOrFail($id);
            $success = true;
            $message = null;

            if (!$messageData->isRead) {
                $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
                $messageData->isRead = true;
                $messageData->editedAt = $dateTime->toDateTimeString();
                $updated = $messageData->save();

                if (!$updated) {
                    return response()->json([
                        'success' => false, 'message' => 'error.databaseError', 'errors' => null
                    ]);
                }
            }
        } catch (ModelNotFoundException $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $messageData = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'messageData' => $messageData,
        ]);
    }

    /**
     * Summary of store
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:3|max:256',
            'email' => 'required|email|max:256',
            'phoneNumber' => 'required|min:3|max:32',
            'phoneCountryCode' => 'required',
            'description' => 'max:4096',
            'radioOption' => 'required',
            'language' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'name.required' => 'validation.requiredField',
            'name.min' => 'validation.mustBeAtLeast3CharsLong',
            'name.max' => 'validation.mustBe256CharsOrFewer',
            'email.required' => 'validation.requiredField',
            'email.email' => 'validation.invalidEmail',
            'email.max' => 'validation.mustBe256CharsOrFewer',
            'phoneNumber.required' => 'validation.requiredField',
            'phoneNumber.min' => 'validation.invalidNumber',
            'phoneNumber.max' => 'validation.invalidNumber',
            'phoneCountryCode.required' => 'validation.invalidNumber',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'language.required' => 'validation.requiredField',
            'radioOption.required' => 'validation.selectOption',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        if (!HelperController::isValidMessageType($request->radioOption)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'radioOption' => ['validation.requiredField'] ],
            ]);
        }

        $message = new Message();
        $message->option = $request->radioOption;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->phoneNumber = $request->phoneNumber;
        $message->phoneCountry = $request->phoneCountryCode;
        $message->description = $request->description;
        $message->clientLanguage = $request->language;
        $created = $message->save();

        if (!$created) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $data = [
            'messageId' => $message->id,
            'messageOption' => self::MESSAGE_OPTIONS[$message->option]['lv'],
            'senderName' => $message->name,
            'senderEmail' => $message->email,
            'senderPhone' => $message->phoneNumber,
            'messageDescription' => $message->description,
            'requisites' => self::getRequisites(),
        ];

        $isMailSent = self::sendEmail(new NewMail($data), MailerType::NOREPLY);

        if ($isMailSent) {
            $message->isSentToExternalMail = true;
            $message->save();
        }

        return response()->json([
            'success' => true, 'message' => 'messageSent', 'errors' => null
        ]);
    }

    /**
     * Summary of reply
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply(Request $request)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::REPLY_ON_MESSAGES)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $rules = [
            'id' => 'required|exists:messages',
            'replyText' => 'required|min:8|max:4096',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'replyText.required' => 'validation.requiredField',
            'replyText.min' => 'validation.mustBeAtLeast8CharsLong',
            'replyText.max' => 'validation.mustBe4096CharsOrFewer',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        /** @var Message $message */
        $message = Message::find($request->id);
        if (!$message->isAnswered) {
            $message->isAnswered = true;
            $message->editedAt = $dateTime->toDateTimeString();
            $updated = $message->save();

            if (!$updated) {
                return response()->json([
                    'success' => false, 'message' => 'error.databaseError', 'errors' => null
                ]);
            }
        }

        $reply = new Reply();
        $reply->messageId = $message->id;
        $reply->description = $request->replyText;
        $created = $reply->save();

        if (!$created) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $data = [
            'to' => $message->email,
            'senderName' => $message->name,
            'messageId' => $message->id,
            'messageOption' => self::MESSAGE_OPTIONS[$message->option][$message->clientLanguage],
            'messageDescription' => $message->description,
            'replyDescription' => $reply->description,
            'requisites' => self::getRequisites(),
        ];

        $returnMessage = 'messageSent';
        $isMailSent = self::sendEmail(new NewReply($data), MailerType::CONTACT);

        if ($isMailSent) {
            $reply->isSent = true;
            $reply->save();
        } else {
            $returnMessage = 'error.failedToSendMessage';
        }

        return response()->json([
            'success' => $isMailSent, 'message' => $returnMessage, 'errors' => null
        ]);
    }

    /**
     * Summary of resend
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend(Request $request)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::REPLY_ON_MESSAGES)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $rules = [
            'id' => 'required|exists:messages',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        /** @var Message $message */
        $message = Message::find($request->id);

        if ($message->isSentToExternalMail) {
            return response()->json([
                'success' => false,
                'message' => 'error.failedToSendMessage',
                'errors' => null,
            ]);
        }

        $data = [
            'messageId' => $message->id,
            'messageOption' => self::MESSAGE_OPTIONS[$message->option]['lv'],
            'senderName' => $message->name,
            'senderEmail' => $message->email,
            'senderPhone' => $message->phoneNumber,
            'messageDescription' => $message->description,
            'requisites' => self::getRequisites(),
        ];

        $isMailSent = self::sendEmail(new NewMail($data), MailerType::NOREPLY);

        if (!$isMailSent) {
            return response()->json([
                'success' => false,
                'message' => 'error.failedToSendMessage',
                'errors' => null
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        $message->isSentToExternalMail = true;
        $message->editedAt = $dateTime->toDateTimeString();
        $updated = $message->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'messageSent', 'errors' => null
        ]);
    }

    /**
     * Summary of sendEmail
     * @param Mailable $mailTemplate
     * @return bool
     */
    public static function sendEmail(
        Mailable $mailTemplate,
        string $mailer
    ): bool
    {
        if (config('app.env') !== 'production') {
            return true;
        }

        try {
            Mail::mailer($mailer)->send($mailTemplate);
        } catch(\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Summary of getRequisites
     * @return array<string, string>
     */
    public static function getRequisites(): array
    {
        // TODO: fix data
        return [
            'name' => 'Stickfix, SIA',
            'regNo' => 'xxx',
            'pvnRegNo' => 'xxx',
            'address' => 'Tallinas iela 28, Rīga, LV-1001',
            'email' => 'contact@stickfix.store',
            'phone' => '+371 28 885 194',
        ];
    }
}
