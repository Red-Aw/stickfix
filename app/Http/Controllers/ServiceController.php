<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ReCaptcha3;
use App\Models\Service;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of ServiceController
 */
class ServiceController extends Controller
{
    /**
     * Summary of getServices
     * @return \Illuminate\Http\JsonResponse
     */
    public function getServices()
    {
        try {
            if (Auth::check()) {
                $services = Service::orderBy('createdAt', 'asc')->get();
            } else {
                if (Cache::has('getServicesGuest')) {
                    $services = Cache::get('getServicesGuest');
                } else {
                    $services = Service::where('isActive', true)->orderBy('createdAt', 'asc')->get();
                    Cache::put('getServicesGuest', $services, now()->addMinutes(5));
                }
            }

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $services = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'services' => $services,
        ]);
    }

    /**
     * Summary of getService
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getService(int $id)
    {
        try {
            $service = Service::findOrFail($id);
            $success = true;
            $message = null;
        } catch (ModelNotFoundException $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $service = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'service' => $service,
        ]);
    }

    /**
     * Summary of store
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'serviceType' => 'required',
            'title' => 'required|min:3|max:256',
            'note' => 'min:3|max:256|nullable',
            'language' => 'string|max:2|nullable',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'isActive' => 'boolean',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'serviceType.required' => 'validation.requiredField',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'language.string' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'note.min' => 'validation.mustBeAtLeast3CharsLong',
            'note.max' => 'validation.mustBe256CharsOrFewer',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbersOrNumbersWithDot',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'isActive.boolean' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        if (!HelperController::isValidPrice($request->price)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'price' => ['validation.mayContainOnlyNaturalNumbersOrNumbersWithDot'] ],
            ]);
        }

        if (!HelperController::isValidServiceType($request->serviceType)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'serviceType' => ['validation.requiredField'] ],
            ]);
        }

        $preparedLanguageValue = null;
        if (HelperController::isValidLanguageType($request->language)) {
            $preparedLanguageValue = $request->language;
        }

        $service = new Service;
        $service->serviceType = $request->serviceType;
        $service->title = $request->title;
        $service->note = $request->note;
        $service->language = $preparedLanguageValue;
        $service->priceInMinorUnit = intval($request->price * 100);
        $service->isActive = $request->isActive;
        $created = $service->save();

        if (!$created) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'recordAdded', 'errors' => null
        ]);
    }

    /**
     * Summary of mark
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function mark(int $id)
    {
        try {
            /** @var Service $service */
            $service = Service::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $service->isActive = !$service->isActive;
        $updated = $service->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of delete
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        try {
            /** @var Service $service */
            $service = Service::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $deleted = $service->delete();

        if (!$deleted) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'recordRemoved', 'errors' => null
        ]);
    }


    /**
     * Summary of edit
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:services',
            'serviceType' => 'required',
            'title' => 'required|min:3|max:256',
            'note' => 'min:3|max:256|nullable',
            'language' => 'string|max:2|nullable',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'isActive' => 'boolean',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'serviceType.required' => 'validation.requiredField',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'note.min' => 'validation.mustBeAtLeast3CharsLong',
            'note.max' => 'validation.mustBe256CharsOrFewer',
            'language.string' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbersOrNumbersWithDot',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'isActive.boolean' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        if (!HelperController::isValidPrice($request->price)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'price' => ['validation.mayContainOnlyNaturalNumbersOrNumbersWithDot'] ],
            ]);
        }

        if (!HelperController::isValidServiceType($request->serviceType)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'serviceType' => ['validation.requiredField'] ],
            ]);
        }

        $preparedLanguageValue = null;
        if (HelperController::isValidLanguageType($request->language)) {
            $preparedLanguageValue = $request->language;
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        /** @var Service $service */
        $service = Service::find($request->id);
        $service->serviceType = $request->serviceType;
        $service->title = $request->title;
        $service->note = $request->note;
        $service->language = $preparedLanguageValue;
        $service->priceInMinorUnit = intval($request->price * 100);
        $service->isActive = $request->isActive;
        $service->editedAt = $dateTime->toDateTimeString();
        $updated = $service->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }
}
