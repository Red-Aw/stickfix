<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

/**
 * Summary of RolesController
 */
class RolesController extends Controller
{
    const SUPER_ADMIN = 'superAdmin';
    const ADD_NEW_USER = 'addNewUser';
    const SEE_STATISTICS = 'seeStatistics'; // TODO: use
    const SEE_TABLES = 'seeTables';
    const ENTER_CONTROL_PANEL = 'enterControlPanel';
    const MARK_USER = 'markUser';
    const EDIT_USER = 'editUser';
    const READ_MESSAGES = 'readMessages';
    const REPLY_ON_MESSAGES = 'replyOnMessages';
    const CONFIRM_USER = 'confirmUser';
    const SEE_ORDERS = 'seeOrders';
    const CHANGE_ORDER_STATUS = 'changeOrderStatus';
    const SEE_SHIPPING_DATA = 'seeShippingData';
    const ADD_OR_EDIT_SHIPPING_DATA = 'addOrEditShippingData';

    const ROLES = array(
        self::SUPER_ADMIN,
        self::ADD_NEW_USER,
        self::SEE_STATISTICS,
        self::SEE_TABLES,
        self::ENTER_CONTROL_PANEL,
        self::MARK_USER,
        self::EDIT_USER,
        self::READ_MESSAGES,
        self::REPLY_ON_MESSAGES,
        self::CONFIRM_USER,
        self::SEE_ORDERS,
        self::CHANGE_ORDER_STATUS,
        self::SEE_SHIPPING_DATA,
        self::ADD_OR_EDIT_SHIPPING_DATA,
    );

    /**
     * Summary of storeNewUserRoles
     * @param array<int, string> $roles
     * @param int $userId
     * @return array<string, mixed>
     */
    public static function storeNewUserRoles(array $roles, int $userId): array
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = Auth::user();
        $authenticatedUserRoles = Role::where('userId', $authenticatedUser->id)->get();

        $roleErrors = [];

        foreach ($roles as $key => $chosenRole) {
            if (!self::isValidRole($chosenRole)) {
                continue;
            }

            if (!self::checkIfUserHasRole($authenticatedUserRoles, $chosenRole)) {
                $roleErrors['roles'][] = [ $key => 'error.userHasNoPermissione' ];
                continue;
            }

            $created = self::storeRole($chosenRole, $userId);

            if (!$created) {
                $roleErrors['roles'][] = [ $key => 'validation.errorAddingRole' ];
                continue;
            }
        }

        return $roleErrors;
    }

    /**
     * Summary of storeExistingUserRoles
     * @param array<int, string> $roles
     * @param int $userId
     * @return array<string, mixed>
     */
    public static function storeExistingUserRoles(array $roles, int $userId): array
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = Auth::user();
        $authenticatedUserRoles = Role::where('userId', $authenticatedUser->id)->get();

        $roleErrors = [];
        $userActiveRoles = Role::where('userId', $userId)->get();

        $listOfRoleIdsThatNeedToBeDeleted = [];
        foreach ($userActiveRoles as $record) {
            $listOfRoleIdsThatNeedToBeDeleted[] = $record['id'];
        }

        foreach ($roles as $key => $chosenRole) {
            if (!self::isValidRole($chosenRole)) {
                continue;
            }

            if (!self::checkIfUserHasRole($authenticatedUserRoles, $chosenRole)) {
                $roleErrors['roles'][] = [ $key => 'error.userHasNoPermission' ];
                continue;
            }

            $found = false;
            foreach ($userActiveRoles as $record) {
                if ($record['role'] === $chosenRole) {
                    $listOfRoleIdsThatNeedToBeDeleted = array_diff($listOfRoleIdsThatNeedToBeDeleted, [ $record['id'] ]);
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $created = self::storeRole($chosenRole, $userId);

                if (!$created) {
                    $roleErrors['roles'][] = [ $key => 'validation.errorAddingRole' ];
                    continue;
                }
            }
        }

        foreach ($listOfRoleIdsThatNeedToBeDeleted as $deletableId) {
            try {
                /** @var Role $role */
                $role = Role::findOrFail($deletableId);
            } catch (ModelNotFoundException $e) {
                $roleErrors['roles'][] = [ $deletableId => 'validation.errorDeletingRole' ];
                continue;
            }

            $deleted = $role->delete();

            if (!$deleted) {
                $roleErrors['roles'][] = [ $deletableId => 'validation.errorDeletingRole' ];
                continue;
            }
        }

        return $roleErrors;
    }

    /**
     * Summary of checkIfAuthenticatedUserHasRole
     * @param string $role
     * @return bool
     */
    public static function checkIfAuthenticatedUserHasRole(string $role): bool
    {
        if (Auth::check()) {
            /** @var User $authenticatedUser */
            $authenticatedUser = Auth::user();
            $authenticatedUserRoles = Role::where('userId', $authenticatedUser->id)->get();

            return self::checkIfUserHasRole($authenticatedUserRoles, $role);
        }

        return false;
    }

    /**
     * Summary of checkIfUserHasRole
     * @param Collection<int, Role> $userRoles
     * @param string $role
     * @return bool
     */
    public static function checkIfUserHasRole(Collection $userRoles, string $role): bool
    {
        foreach ($userRoles as $userRole) {
            if ($userRole['role'] === $role || $userRole['role'] === self::SUPER_ADMIN) {
                return true;
            }
        }
        return false;
    }

    /**
     * Summary of isValidRole
     * @param string $role
     * @return bool
     */
    private static function isValidRole(string $role): bool
    {
        return in_array($role, self::ROLES);
    }

    /**
     * Summary of storeRole
     * @param string $role
     * @param int $userId
     * @return bool
     */
    private static function storeRole($role, $userId): bool
    {
        $roleObj = new Role();
        $roleObj->userId = $userId;
        $roleObj->role = $role;
        $created = $roleObj->save();

        return $created;
    }
}
