<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ReCaptcha3;
use App\Models\Carousel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of CarouselController
 */
class CarouselController extends Controller
{
    /**
     * Summary of getItems
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItems()
    {
        try {
            if (Auth::check()) {
                $items = Carousel::orderBy('createdAt', 'desc')->get();
            } else {
                if (Cache::has('getCarouselItemsGuest')) {
                    $items = Cache::get('getCarouselItemsGuest');
                } else {
                    $items = Carousel::where('isActive', true)->orderBy('createdAt', 'desc')->get();
                    Cache::put('getCarouselItemsGuest', $items, now()->addMinutes(5));
                }
            }

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $items = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'items' => $items,
        ]);
    }

    /**
     * Summary of getItem
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItem(int $id)
    {
        try {
            $item = Carousel::findOrFail($id);
            $success = true;
            $message = null;
        } catch (ModelNotFoundException $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $item = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'item' => $item,
        ]);
    }

    /**
     * Summary of store
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'max:256',
            'language' => 'string|max:2|nullable',
            'isActive' => 'boolean',
            'image' => 'required',
            'image.base64' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'language.string' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isActive.boolean' => 'validation.requiredField',
            'image.required' => 'validation.requiredField',
            'image.base64' => [
                'required' => 'validation.requiredField',
            ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        $preparedLanguageValue = null;
        if (HelperController::isValidLanguageType($request->language)) {
            $preparedLanguageValue = $request->language;
        }

        $carousel = new Carousel();
        $carousel->title = $request->title ?? '';
        $carousel->language = $preparedLanguageValue;
        $carousel->imageName = '';
        $carousel->imagePath = '';
        $carousel->isActive = $request->isActive;
        $created = $carousel->save();

        if (!$created) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
        $path = ImageController::getObjectPath(ImageController::CAROUSEL_FOLDER_NAME, $carousel->id);
        $result = ImageController::prepareAndStoreImage(
            $request->image['base64'],
            $path,
            $carousel->id,
            $dateTime->timestamp
        );

        $imageName = $result['imageName'];
        $receivedErrorList = $result['errorList'];

        $imageErrors = [];
        if (is_array($receivedErrorList) && count($receivedErrorList) > 0) {
            $carousel->delete();
            $imageErrors['image'] = $receivedErrorList[0];
            return response()->json([
                'success' => false, 'message' => 'validation.errorAddingImage', 'errors' => $imageErrors
            ]);
        }

        if (is_null($imageName)) {
            $carousel->delete();
            $imageErrors['image'] = [ 'validation.errorAddingImage' ];
            return response()->json([
                'success' => false, 'message' => 'validation.errorAddingImage', 'errors' => $imageErrors
            ]);
        }

        $carousel->imageName = $imageName;
        $carousel->imagePath = ImageController::getUploadPath($path);
        $updated = $carousel->save();

        if (!$updated) {
            $carousel->delete();
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'recordAdded', 'errors' => null
        ]);
    }

    /**
     * Summary of mark
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function mark(int $id)
    {
        try {
            /** @var Carousel $item */
            $item = Carousel::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $item->isActive = !$item->isActive;
        $updated = $item->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of edit
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:carousels',
            'title' => 'max:256',
            'language' => 'string|max:2|nullable',
            'isActive' => 'boolean',
            'isImageReplaced' => 'boolean',
            'newImage' => 'required',
            'newImage.base64' => 'required_if:isImageReplaced,true',
            'storedImage' => 'required',
            'storedImage.path' => 'required',
            'storedImage.name' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'language.string' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isActive.boolean' => 'validation.requiredField',
            'isImageReplaced.boolean' => 'validation.requiredField',
            'newImage.required' => 'validation.requiredField',
            'newImage.base64' => [
                'required_if' => 'validation.requiredField'
            ],
            'storedImage.required' => 'validation.storedImagesDoesNotExist',
            'storedImage.path' => [
                'required' => 'validation.requiredField'
            ],
            'storedImage.name' => [
                'required' => 'validation.requiredField'
            ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        $preparedLanguageValue = null;
        if (HelperController::isValidLanguageType($request->language)) {
            $preparedLanguageValue = $request->language;
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        $imageName = $request->storedImage['name'];
        $imagePath = $request->storedImage['path'];
        $imageErrors = [];
        if ($request->isImageReplaced) {
            $path = ImageController::getObjectPath(ImageController::CAROUSEL_FOLDER_NAME, $request->id);
            $result = ImageController::prepareAndStoreImage(
                $request->newImage['base64'],
                $path,
                $request->id,
                $dateTime->timestamp
            );

            $imageName = $result['imageName'];
            $receivedErrorList = $result['errorList'];

            if (is_array($receivedErrorList) && count($receivedErrorList) > 0) {
                $imageErrors['newImage'] = $receivedErrorList[0];
                return response()->json([
                    'success' => false, 'message' => 'validation.errorAddingImage', 'errors' => $imageErrors
                ]);
            }

            if (is_null($imageName)) {
                $imageErrors['image'] = [ 'validation.errorAddingImage' ];
                return response()->json([
                    'success' => false, 'message' => 'validation.errorAddingImage', 'errors' => $imageErrors
                ]);
            }

            $imagePath = ImageController::getUploadPath($path);

            ImageController::deleteSpecificImage($path . $request->storedImage['name']);
        }

        /** @var Carousel $carousel */
        $carousel = Carousel::find($request->id);
        $carousel->title = $request->title ?? '';
        $carousel->language = $preparedLanguageValue;
        $carousel->imageName = $imageName;
        $carousel->imagePath = $imagePath;
        $carousel->isActive = $request->isActive;
        $carousel->editedAt = $dateTime->toDateTimeString();
        $updated = $carousel->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => $imageErrors
        ]);
    }

    /**
     * Summary of delete
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        try {
            /** @var Carousel $item */
            $item = Carousel::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $deleted = $item->delete();

        if (!$deleted) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        ImageController::deleteAllModelImages(ImageController::MODEL_CAROUSEL, $id);

        return response()->json([
            'success' => true, 'message' => 'recordRemoved', 'errors' => null
        ]);
    }
}
