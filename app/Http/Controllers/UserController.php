<?php

namespace App\Http\Controllers;

use App\Enums\Language;
use App\Http\Controllers\ReCaptcha3;
use App\Http\Controllers\RolesController;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Summary of UserController
 */
class UserController extends Controller
{
    /**
     * Summary of changePass
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePass(Request $request)
    {
        $rules = [
            'id' => 'required|exists:users',
            'currentPassword' => 'required|min:8|max:1024',
            'newPassword' => 'required|min:8|max:1024|confirmed',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'currentPassword.required' => 'validation.requiredField',
            'currentPassword.min' => 'validation.mustBeAtLeast8CharsLong',
            'currentPassword.max' => 'validation.mustBe1024CharsOrFewer',
            'newPassword.required' => 'validation.requiredField',
            'newPassword.min' => 'validation.mustBeAtLeast8CharsLong',
            'newPassword.max' => 'validation.mustBe1024CharsOrFewer',
            'newPassword.confirmed' => 'validation.passwordsDoNotMatch',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors()
            ]);
        }

        /** @var User $user */
        $user = Auth::user();
        if ($user->id !== $request->id) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => null
            ]);
        }

        if (!Hash::check($request->currentPassword, $user->password)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => null
            ]);
        }

        if ($request->currentPassword === $request->newPassword) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => null
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        /** @var User $user */
        $user = User::find($request->id);
        $user->password = Hash::make($request->newPassword);
        $user->editedAt = $dateTime->toDateTimeString();
        $user->passEditedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of changeLanguage
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeLanguage(Request $request)
    {
        $rules = [
            'id' => 'required|exists:users',
            'language' => 'required|min:2|max:2',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'language.required' => 'validation.requiredField',
            'language.min' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors()
            ]);
        }

        /** @var User $user */
        $user = Auth::user();
        if ($user->id !== $request->id) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => null
            ]);
        }

        if (!self::isValidLanguage($request->language)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'language' => ['validation.requiredField'] ],
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        /** @var User $user */
        $user = User::find($request->id);
        $user->language = $request->language;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of checkPasswordMatch
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPasswordMatch(Request $request)
    {
        $rules = [
            'id' => 'required|exists:users',
            'currentPassword' => 'required|min:8|max:1024',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'match' => false
            ]);
        }

        /** @var User $user */
        $user = Auth::user();
        if ($user->id !== $request->id) {
            return response()->json([
                'match' => false
            ]);
        }

        if (!Hash::check($request->currentPassword, $user->password)) {
            return response()->json([
                'match' => false
            ]);
        }

        return response()->json([
            'match' => true
        ]);
    }

    /**
     * Summary of getUsers
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers()
    {
        try {
            if (Auth::check()) {
                $users = User::orderBy('id', 'asc')->get();
            } else {
                throw new Exception();
            }

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $users = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'users' => $users,
        ]);
    }

    /**
     * Summary of getUser
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(int $id)
    {
        try {
            $item = User::with('roles')->findOrFail($id);
            $success = true;
            $message = null;
        } catch (ModelNotFoundException $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $item = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'user' => $item,
        ]);
    }

    /**
     * Summary of getRoles
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoles()
    {
        try {
            if (Auth::check()) {
                /** @var User $user */
                $user = Auth::user();
                $roles = Role::where('userId', $user->id)->get();
            } else {
                throw new Exception();
            }

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $roles = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'roles' => $roles,
        ]);
    }

    /**
     * Summary of getAllRoles
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllRoles()
    {
        return response()->json(RolesController::ROLES);
    }

    /**
     * Summary of block
     * @param int $id
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function block(int $id)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::MARK_USER)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        try {
            if (self::isDefaultUser($id)) {
                throw new Exception();
            }
            /** @var User $user */
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false, 'message' => 'error.error', 'errors' => null
            ]);
        }

        /** @var User $authenticatedUser */
        $authenticatedUser = Auth::user();
        if ($user->id === $authenticatedUser->id) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        $user->isBlocked = !$user->isBlocked;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of edit
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::EDIT_USER)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $rules = [
            'id' => 'required|exists:users',
            'username' => [ 'required', 'min:3', 'max:64', Rule::unique('users')->ignore($request->id) ],
            'originalUsername' => 'required|min:3|max:64|exists:users,username',
            'email' => [ 'required', 'email', 'max:256', Rule::unique('users')->ignore($request->id) ],
            'originalEmail' => 'required|email|max:256|exists:users,email',
            'language' => 'required|min:2|max:2',
            'isBlocked' => 'boolean',
            'roles' => 'array',
            'roles.*' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'username.required' => 'validation.requiredField',
            'username.min' => 'validation.mustBeAtLeast3CharsLong',
            'username.max' => 'validation.mustBe64CharsOrFewer',
            'username.unique' => 'validation.usernameAlreadyExists',
            'originalUsername.required' => 'validation.requiredField',
            'originalUsername.min' => 'validation.mustBeAtLeast3CharsLong',
            'originalUsername.max' => 'validation.mustBe64CharsOrFewer',
            'originalUsername.exists' => 'validation.usernameDoesNotExist',
            'email.required' => 'validation.requiredField',
            'email.email' => 'validation.invalidEmail',
            'email.max' => 'validation.mustBe256CharsOrFewer',
            'email.unique' => 'validation.emailAlreadyExists',
            'originalEmail.required' => 'validation.requiredField',
            'originalEmail.email' => 'validation.invalidEmail',
            'originalEmail.max' => 'validation.mustBe256CharsOrFewer',
            'originalEmail.exists' => 'validation.emailDoesNotExist',
            'language.required' => 'validation.requiredField',
            'language.min' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isBlocked.boolean' => 'validation.requiredField',
            'roles.required' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors()
            ]);
        }

        /** @var User $authenticatedUser */
        $authenticatedUser = Auth::user();
        if (self::isDefaultUser($request->id) && !self::isDefaultUser($authenticatedUser->id)) {
            return response()->json([
                'success' => false, 'message' => 'error.error', 'errors' => null
            ]);
        }

        if (!self::isValidLanguage($request->language)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'language' => ['validation.requiredField'] ],
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        /** @var User $user */
        $user = User::find($request->id);
        $user->username = $request->username;
        $user->email = $request->email;
        $user->language = $request->language;
        if ($authenticatedUser->id !== $request->id) {
            $user->isBlocked = $request->isBlocked;
        }
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $roleErrors = RolesController::storeExistingUserRoles($request->roles, $user->id);

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => $roleErrors
        ]);
    }

    /**
     * Summary of switchShowInactive
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function switchShowInactive()
    {
        try {
            if (!Auth::check()) {
                throw new Exception();
            }

            /** @var User $authenticatedUser */
            $authenticatedUser = Auth::user();

            /** @var User $user */
            $user = User::findOrFail($authenticatedUser->id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false, 'message' => 'error.error', 'errors' => null
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        $user->showInactive = !$user->showInactive;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of switchShowDeleted
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function switchShowDeleted()
    {
        try {
            if (!Auth::check()) {
                throw new Exception();
            }

            /** @var User $authenticatedUser */
            $authenticatedUser = Auth::user();

            /** @var User $user */
            $user = User::findOrFail($authenticatedUser->id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false, 'message' => 'error.error', 'errors' => null
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        $user->showDeleted = !$user->showDeleted;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of isVerificationViewShown
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function isVerificationViewShown(int $userId)
    {
        /** @var User $user */
        $user = User::findOrFail($userId);

        if ($user->hasVerifiedEmail() || $user->isConfirmed) {
            return response()->json([
                'success' => (bool)$user->isShownToUser
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Summary of isDefaultUser
     * @param int $id
     * @return bool
     */
    public static function isDefaultUser(int $id): bool
    {
        return $id === 1 ? true : false;
    }

    /**
     * Summary of isValidLanguage
     * @param string $language
     * @return bool
     */
    public static function isValidLanguage(string $language): bool
    {
        return in_array($language, Language::getValues());
    }
}
