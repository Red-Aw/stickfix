<?php

namespace App\Http\Controllers;

use App\Enums\MailerType;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\ReCaptcha3;
use App\Mail\LowQuantity;
use App\Models\Image;
use App\Models\Item;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of ItemController
 */
class ItemController extends Controller
{
    /**
     * Summary of getItems
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItems()
    {
        try {
            if (Auth::check()) {
                $items = Item::with('images')->orderBy('createdAt', 'desc')->get();
            } else {
                $items = Item::with('images')
                    ->where('isActive', true)
                    ->where('isDeleted', false)
                    ->orderBy('createdAt', 'desc')
                    ->get();
            }

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $items = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'items' => $items,
        ]);
    }

    /**
     * Summary of getCartItems
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCartItems(Request $request)
    {
        $rules = [
            'items' => 'array',
            'items.*.id' => 'required',
        ];

        $messages = [
            'items.required' => 'validation.requiredField',
            'items.*.id' => [
                'required' => 'validation.requiredField',
            ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        if (count($request->items) <= 0) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => 'error.errorSelectingData',
            ]);
        }

        try {
            $itemIdList = [];
            foreach ($request->items as $item) {
                $itemIdList[] = $item['id'];
            }

            $items = Item::with('images')->whereIn('id', $itemIdList)->get();
            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $items = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'items' => $items,
        ]);
    }

    /**
     * Summary of getItem
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItem(int $id)
    {
        try {
            $item = Item::with('images')->findOrFail($id);
            $success = true;
            $message = null;
        } catch (ModelNotFoundException $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $item = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'item' => $item,
        ]);
    }

    /**
     * Summary of store
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'category' => 'required',
            'isActive' => 'boolean',
            'isAvailableForSale' => 'boolean',
            'state' => 'required',
            'brand' => 'nullable',
            'size' => 'nullable',
            'stickFlex' => 'nullable',
            'bladeCurve' => 'nullable',
            'bladeSide' => 'nullable',
            'stickSize' => 'nullable',
            'skateSize' => 'nullable',
            'skateLength' => 'nullable',
            'skateWidth' => 'nullable',
            'gender' => 'nullable',
            'quantity' => 'required|integer|min:0|max:2000000000',
            'isParcelDelivery' => 'boolean',
            'imageCollection' => 'array',
            'imageCollection.*.base64' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbersOrNumbersWithDot',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'category.required' => 'validation.requiredField',
            'isActive.boolean' => 'validation.requiredField',
            'isAvailableForSale.boolean' => 'validation.requiredField',
            'state.required' => 'validation.requiredField',
            'quantity.required' => 'validation.requiredField',
            'quantity.integer' => 'validation.mayContainOnlyNaturalNumbers',
            'quantity.min' => 'validation.mustBeAPositiveValue',
            'quantity.max' => 'validation.numberTooLarge',
            'isParcelDelivery.boolean' => 'validation.requiredField',
            'imageCollection.required' => 'validation.requiredField',
            'imageCollection.*.base64' => [
                'required' => 'validation.requiredField',
            ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        if (!HelperController::isValidPrice($request->price)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'price' => ['validation.mayContainOnlyNaturalNumbersOrNumbersWithDot'] ],
            ]);
        }

        if (!HelperController::isValidCategory($request->category)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'category' => ['validation.requiredField'] ],
            ]);
        }

        if (!HelperController::isValidCondition($request->state)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'state' => ['validation.requiredField'] ],
            ]);
        }

        $brandKey = null;
        if ($request->brand) {
            $brandKey = $request->brand;

            if (!HelperController::isValidBrand($request->brand)) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.validationError',
                    'errors' => [ 'brand' => ['validation.requiredField'] ],
                ]);
            }
        }

        if ($request->isActive && $request->isAvailableForSale) {
            if (intval($request->quantity) < 1) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.validationError',
                    'errors' => [ 'quantity' => ['validation.requiredField'] ],
                ]);
            }
        }

        $genderKey = null;
        if ($request->gender) {
            $genderKey = $request->gender;

            if (!HelperController::isValidGender($genderKey)) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.validationError',
                    'errors' => [ 'gender' => ['validation.requiredField'] ],
                ]);
            }
        }

        $itemSizes = $this->getItemSizes(
            $request->category,
            array(
                'size' => $request->size,
                'stickSize' => $request->stickSize,
                'stickFlex' => $request->stickFlex,
                'bladeCurve' => $request->bladeCurve,
                'bladeSide' => $request->bladeSide,
                'skateSize' => $request->skateSize,
                'skateLength' => $request->skateLength,
                'skateWidth' => $request->skateWidth,
            ),
        );

        $quantity = intval($request->quantity);
        $isActive = $request->isActive;
        if ($request->isAvailableForSale) {
            if ($quantity <= 0) {
                $isActive = false;
                $quantity = 0;
            }
        } else {
            $quantity = 0;
        }

        /** @var User $user */
        $user = Auth::user();
        $item = new Item;
        $item->userId = $user->id;
        $item->title = $request->title;
        $item->description = $request->description;
        $item->priceInMinorUnit = intval($request->price * 100);
        $item->category = $request->category;
        $item->isActive = $isActive;
        $item->isAvailableForSale = $request->isAvailableForSale;
        $item->state = $request->state;
        $item->brand = $brandKey;
        $item->size = $itemSizes['size'];
        $item->stickSize = $itemSizes['stickSize'];
        $item->stickFlex = $itemSizes['stickFlex'];
        $item->bladeCurve = $itemSizes['bladeCurve'];
        $item->bladeSide = $itemSizes['bladeSide'];
        $item->skateSize = $itemSizes['skateSize'];
        $item->skateLength = $itemSizes['skateLength'];
        $item->skateWidth = $itemSizes['skateWidth'];
        $item->quantity = $quantity;
        $item->gender = $genderKey;
        $item->isParcelDelivery = $request->isParcelDelivery;
        $created = $item->save();

        if (!$created) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $path = ImageController::getObjectPath(ImageController::ITEMS_FOLDER_NAME, $item->id);
        $imageErrors = ImageController::store($request->imageCollection, $path, $item);

        return response()->json([
            'success' => true, 'message' => 'recordAdded', 'errors' => $imageErrors
        ]);
    }

    /**
     * Summary of mark
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function mark(int $id)
    {
        try {
            /** @var Item $item */
            $item = Item::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        if ($item->isDeleted) {
            return response()->json([
                'success' => false, 'message' => 'error.forbiddenToEditDeletedEntry', 'errors' => null
            ]);
        }

        $status = !$item->isActive;
        if ($status && $item->isAvailableForSale && $item->quantity < 1) {
            return response()->json([
                'success' => false, 'message' => 'error.quantityShouldBeLargerThanZero', 'errors' => null
            ]);
        }

        $item->isActive = $status;
        $updated = $item->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of delete
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        try {
            /** @var Item $item */
            $item = Item::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $status = !$item->isDeleted;
        $item->isDeleted = $status;
        if (!$status) {
            $item->isActive = false;
        }
        $updated = $item->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of edit
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:items',
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'category' => 'required',
            'isActive' => 'boolean',
            'isAvailableForSale' => 'boolean',
            'state' => 'required',
            'brand' => 'nullable',
            'size' => 'nullable',
            'stickFlex' => 'nullable',
            'bladeCurve' => 'nullable',
            'bladeSide' => 'nullable',
            'stickSize' => 'nullable',
            'skateSize' => 'nullable',
            'skateLength' => 'nullable',
            'skateWidth' => 'nullable',
            'gender' => 'nullable',
            'quantity' => 'required|integer|min:0|max:2000000000',
            'isParcelDelivery' => 'boolean',
            'newImageCollection' => 'array',
            'newImageCollection.*.base64' => 'required',
            'storedImageCollection' => 'array',
            'storedImageCollection.*.id' => 'required|exists:images',
            'storedImageCollection.*.path' => 'required',
            'storedImageCollection.*.name' => 'required',
            'deletedImageCollection' => 'array',
            'deletedImageCollection.*.id' => 'required|exists:images',
            'deletedImageCollection.*.path' => 'required',
            'deletedImageCollection.*.name' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbersOrNumbersWithDot',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'category.required' => 'validation.requiredField',
            'isActive.boolean' => 'validation.requiredField',
            'isAvailableForSale.boolean' => 'validation.requiredField',
            'state.required' => 'validation.requiredField',
            'quantity.required' => 'validation.requiredField',
            'quantity.integer' => 'validation.mayContainOnlyNaturalNumbers',
            'quantity.min' => 'validation.mustBeAPositiveValue',
            'quantity.max' => 'validation.numberTooLarge',
            'isParcelDelivery.boolean' => 'validation.requiredField',
            'newImageCollection.required' => 'validation.requiredField',
            'newImageCollection.*.base64' => [ 'required' => 'validation.requiredField' ],
            'storedImageCollection.required' => 'validation.storedImagesDoesNotExist',
            'storedImageCollection.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'storedImagesIdentifierDoesNotExist'
            ],
            'storedImageCollection.*.path' => [ 'required' => 'validation.requiredField' ],
            'storedImageCollection.*.name' => [ 'required' => 'validation.requiredField' ],
            'deletedImageCollection.required' => 'validation.deletedImagesDoesNotExist',
            'deletedImageCollection.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'deletedImagesIdentifierDoesNotExist'
            ],
            'deletedImageCollection.*.path' => [ 'required' => 'validation.requiredField' ],
            'deletedImageCollection.*.name' => [ 'required' => 'validation.requiredField' ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        /** @var Item $item */
        $item = Item::find($request->id);

        if ($item->isDeleted) {
            return response()->json([
                'success' => false, 'message' => 'error.forbiddenToEditDeletedEntry', 'errors' => null
            ]);
        }

        if (!HelperController::isValidPrice($request->price)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'price' => ['validation.mayContainOnlyNaturalNumbersOrNumbersWithDot'] ],
            ]);
        }

        if (!HelperController::isValidCategory($request->category)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'category' => ['validation.requiredField'] ],
            ]);
        }

        if (!HelperController::isValidCondition($request->state)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'state' => ['validation.requiredField'] ],
            ]);
        }

        $brandKey = null;
        if ($request->brand) {
            $brandKey = $request->brand;

            if (!HelperController::isValidBrand($request->brand)) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.validationError',
                    'errors' => [ 'brand' => ['validation.requiredField'] ],
                ]);
            }
        }

        if ($request->isActive && $request->isAvailableForSale) {
            if (intval($request->quantity) < 1) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.validationError',
                    'errors' => [ 'quantity' => ['validation.requiredField'] ],
                ]);
            }
        }

        $genderKey = null;
        if ($request->gender) {
            $genderKey = $request->gender;

            if (!HelperController::isValidGender($request->gender)) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.validationError',
                    'errors' => [ 'gender' => ['validation.requiredField'] ],
                ]);
            }
        }

        $itemSizes = $this->getItemSizes(
            $request->category,
            array(
                'size' => $request->size,
                'stickSize' => $request->stickSize,
                'stickFlex' => $request->stickFlex,
                'bladeCurve' => $request->bladeCurve,
                'bladeSide' => $request->bladeSide,
                'skateSize' => $request->skateSize,
                'skateLength' => $request->skateLength,
                'skateWidth' => $request->skateWidth,
            ),
        );

        $quantity = intval($request->quantity);
        $isActive = $request->isActive;
        if ($request->isAvailableForSale) {
            if ($quantity <= 0) {
                $isActive = false;
                $quantity = 0;
            }
        } else {
            $quantity = 0;
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        /** @var User $user */
        $user = Auth::user();
        $item->userId = $user->id;
        $item->title = $request->title;
        $item->description = $request->description;
        $item->priceInMinorUnit = intval($request->price * 100);
        $item->category = $request->category;
        $item->isActive = $isActive;
        $item->isAvailableForSale = $request->isAvailableForSale;
        $item->state = $request->state;
        $item->brand = $brandKey;
        $item->size = $itemSizes['size'];
        $item->stickSize = $itemSizes['stickSize'];
        $item->stickFlex = $itemSizes['stickFlex'];
        $item->bladeCurve = $itemSizes['bladeCurve'];
        $item->bladeSide = $itemSizes['bladeSide'];
        $item->skateSize = $itemSizes['skateSize'];
        $item->skateLength = $itemSizes['skateLength'];
        $item->skateWidth = $itemSizes['skateWidth'];
        $item->quantity = $quantity;
        $item->gender = $genderKey;
        $item->isParcelDelivery = $request->isParcelDelivery;
        $item->editedAt = $dateTime->toDateTimeString();
        $updated = $item->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $path = ImageController::getObjectPath(ImageController::ITEMS_FOLDER_NAME, $item->id);
        ImageController::deleteSelectedImages($request->deletedImageCollection, $path);
        $imageErrors = ImageController::store($request->newImageCollection, $path, $item);

        if ($request->isAvailableForSale && $quantity <= 0) {
            $image = Image::where(ImageController::MODEL_ITEM_ID, $item->id)->first();

            $imageUrl = null;
            $isImageAvailable = false;
            if ($image) {
                $imageUrl = $image['path'] . $image['name'];
                $isImageAvailable = true;
            }

            $mailData = [
                'itemId' => $item->id,
                'itemTitle' => $item->title,
                'itemPrice' => $request->price,
                'imageUrl' => $imageUrl,
                'isImageAvailable' => $isImageAvailable,
                'requisites' => MessageController::getRequisites(),
            ];

            MessageController::sendEmail(new LowQuantity($mailData), MailerType::NOREPLY);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => $imageErrors
        ]);
    }

    /**
     * Summary of getItemSizes
     * @param string $categoryValue
     * @param array<string, mixed> $providedSizes
     * @return array<string, mixed>
     */
    private function getItemSizes(string $categoryValue, array $providedSizes): array
    {
        $itemSizes = array(
            'size' => null,
            'stickSize' => null,
            'stickFlex' => null,
            'bladeCurve' => null,
            'bladeSide' => null,
            'skateSize' => null,
            'skateLength' => null,
            'skateWidth' => null,
        );

        if ($categoryValue === 'SKATES') {
            $itemSizes['skateSize'] = $providedSizes['skateSize'];
            $itemSizes['skateLength'] = $providedSizes['skateLength'];
            $itemSizes['skateWidth'] = $providedSizes['skateWidth'];
        } else if ($categoryValue === 'STICKS') {
            $itemSizes['stickSize'] = $providedSizes['stickSize'];
            $itemSizes['stickFlex'] = $providedSizes['stickFlex'];
            $itemSizes['bladeCurve'] = $providedSizes['bladeCurve'];
            $itemSizes['bladeSide'] = $providedSizes['bladeSide'];
        } else if ($categoryValue === 'PANTS'
            || $categoryValue === 'SHOULDER_PADS'
            || $categoryValue === 'ELBOW_PADS'
        ) {
            $itemSizes['size'] = $providedSizes['size']['value'] ?? null;
        } else {
            $itemSizes['size'] = $providedSizes['size'];
        }

        return $itemSizes;
    }
}
