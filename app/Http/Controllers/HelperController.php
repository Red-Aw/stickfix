<?php

namespace App\Http\Controllers;
use App\Enums\BrandType;
use App\Enums\CategoryType;
use App\Enums\ConditionType;
use App\Enums\CountryCode;
use App\Enums\DeliveryType;
use App\Enums\GenderType;
use App\Enums\Language;
use App\Enums\MessageType;
use App\Enums\OrderStatus;
use App\Enums\ServiceType;

/**
 * Summary of HelperController
 */
class HelperController extends Controller
{
    /**
     * Summary of isValidLanguageType
     * @param string $language
     * @return bool
     */
    public static function isValidLanguageType($language)
    {
        return in_array($language, Language::getValues());
    }

        /**
     * Summary of isValidPrice
     * @param float $price
     * @return bool
     */
    public static function isValidPrice(float $price): bool
    {
        return is_float(floatval($price));
    }

    /**
     * Summary of isValidCondition
     * @param string $condition
     * @return bool
     */
    public static function isValidCondition(string $condition): bool
    {
        return in_array($condition, ConditionType::getValues());
    }

    /**
     * Summary of isValidCategory
     * @param string $category
     * @return bool
     */
    public static function isValidCategory(string $category): bool
    {
        return in_array($category, CategoryType::getValues());
    }

    /**
     * Summary of isValidBrand
     * @param string $brand
     * @return bool
     */
    public static function isValidBrand(string $brand): bool
    {
        return in_array($brand, BrandType::getValues());
    }

    /**
     * Summary of isValidGender
     * @param string $gender
     * @return bool
     */
    public static function isValidGender(string $gender): bool
    {
        return in_array($gender, GenderType::getValues());
    }

    /**
     * Summary of isValidMessageType
     * @param string $option
     * @return bool
     */
    public static function isValidMessageType(string $option)
    {
        return in_array($option, MessageType::getValues());
    }

    /**
     * Summary of isValidDeliveryType
     * @param string $deliveryType
     * @return bool
     */
    public static function isValidDeliveryType(string $deliveryType): bool
    {
        return in_array($deliveryType, DeliveryType::getValues());
    }

    /**
     * Summary of isValidCountryCode
     * @param string $countryCode
     * @return bool
     */
    public static function isValidCountryCode(string $countryCode): bool
    {
        return in_array($countryCode, CountryCode::getValues());
    }

    /**
     * Summary of isParcelDeliveryType
     * @param string $deliveryType
     * @return bool
     */
    public static function isParcelDeliveryType(string $deliveryType): bool
    {
        return in_array($deliveryType, [ DeliveryType::OMNIVA ]);
    }

    /**
     * Summary of isCourierDeliveryType
     * @param string $deliveryType
     * @return bool
     */
    public static function isCourierDeliveryType(string $deliveryType): bool
    {
        return in_array($deliveryType, [ DeliveryType::COURIER ]);
    }

    /**
     * Summary of isRestrictedDeliveryType
     * @param string $deliveryType
     * @return bool
     */
    public static function isRestrictedDeliveryType(string $deliveryType): bool
    {
        return in_array($deliveryType, [ DeliveryType::COURIER, DeliveryType::PICK_UP_IN_STORE_FOR_FREE ]);
    }

    /**
     * Summary of isValidOrderStatus
     * @param string $orderStatus
     * @return bool
     */
    public static function isValidOrderStatus(string $orderStatus)
    {
        return in_array($orderStatus, OrderStatus::getValues());
    }

    /**
     * Summary of isValidServiceType
     * @param string $serviceType
     * @return bool
     */
    public static function isValidServiceType(string $serviceType)
    {
        return in_array($serviceType, ServiceType::getValues());
    }
}
