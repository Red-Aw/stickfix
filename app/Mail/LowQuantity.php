<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Summary of LowQuantity
 */
class LowQuantity extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Summary of data
     * @var mixed
     */
    public $data;

    /**
     * Summary of __construct
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Summary of build
     * @return LowQuantity
     */
    public function build()
    {
        return $this->view('emails.lowQuantity')
            ->to(config('mail.notification.address'), config('mail.notification.name'))
            ->subject('Stickfix.store - Preces (' . $this->data['itemTitle'] . ') daudzums samazinājies līdz nullei')
            ->from(config('mail.noreply.address'), config('mail.noreply.name'))
            ->with('data', $this->data);
    }
}
