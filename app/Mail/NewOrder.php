<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Summary of NewOrder
 */
class NewOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Summary of data
     * @var mixed
     */
    public $data;

    /**
     * Summary of __construct
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Summary of build
     * @return NewOrder
     */
    public function build()
    {
        return $this->view('emails.newOrder')
            ->to($this->data['clientEmail'], $this->data['clientName'])
            ->subject('Stickfix.store - Saņemta apmaksa par pasūtījumu')
            ->from(config('mail.noreply.address'), config('mail.noreply.name'))
            ->with('data', $this->data);
    }
}
