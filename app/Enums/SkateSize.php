<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SR()
 * @method static static INT()
 * @method static static JR()
 * @method static static YTH()
 *
 * @extends Enum<string>
 */
final class SkateSize extends Enum
{
    const SR = 'SR';
    const INT = 'INT';
    const JR = 'JR';
    const YTH = 'YTH';
}
