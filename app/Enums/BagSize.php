<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SR()
 * @method static static JR()
 *
 * @extends Enum<string>
 */
final class BagSize extends Enum
{
    const SR = 'SR';
    const JR = 'JR';
}
