<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static C()
 * @method static static D()
 * @method static static E()
 * @method static static EE()
 * @method static static R()
 * @method static static FIT_1()
 * @method static static FIT_2()
 * @method static static FIT_3()
 * @method static static TAPERED()
 * @method static static REGULAR()
 * @method static static WIDE()
 *
 * @extends Enum<string>
 */
final class SkateWidthSize extends Enum
{
    const C = 'C';
    const D = 'D';
    const E = 'E';
    const EE = 'EE';
    const R = 'R';
    const FIT_1 = 'Fit 1';
    const FIT_2 = 'Fit 2';
    const FIT_3 = 'Fit 3';
    const TAPERED = 'Tapered';
    const REGULAR = 'Regular';
    const WIDE = 'Wide';
}
