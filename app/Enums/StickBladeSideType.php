<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LEFT()
 * @method static static RIGHT()
 * @method static static NONE()
 *
 * @extends Enum<string>
 */
final class StickBladeSideType extends Enum
{
    const LEFT = 'LEFT';
    const RIGHT = 'RIGHT';
    const NONE = 'NONE';
}
