<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static INCOMPLETE()
 * @method static static PAID()
 * @method static static FAILED()
 * @method static static SENT_OUT()
 * @method static static FINISHED()
 *
 * @extends Enum<string>
 */
final class OrderStatus extends Enum
{
    const INCOMPLETE = 'incomplete';
    const PAID = 'paid';
    const FAILED = 'failed';
    const SENT_OUT = 'sentOut';
    const FINISHED = 'finished';
}
