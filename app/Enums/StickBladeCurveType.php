<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static P28()
 * @method static static P92()
 * @method static static P29()
 * @method static static P88()
 *
 * @extends Enum<string>
 */
final class StickBladeCurveType extends Enum
{
    const P28 = 'P28';
    const P92 = 'P92';
    const P29 = 'P29';
    const P88 = 'P88';
    const P02 = 'P02';
    const P03 = 'P03';
    const P08 = 'P08';
    const PM9 = 'PM9';
    const P14 = 'P14';
    const P15 = 'P15';
    const P17 = 'P17';
    const P19 = 'P19';
    const P33 = 'P33';
    const P36A = 'P36A';
    const P38 = 'P38';
    const P40 = 'P40';
    const P42 = 'P42';
    const P45 = 'P45';
    const P46 = 'P46';
    const P87A = 'P87A';
    const P91A = 'P91A';
    const P106 = 'P106';
    const TC2 = 'TC2';
    const TC2_5 = 'TC2.5';
    const TC3 = 'TC3';
    const TC4 = 'TC4';
    const MC = 'MC';
    const MC2 = 'MC2';
    const HCR = 'HCR';
    const HCS = 'HCS';
    const W01 = 'W01';
    const W02 = 'W02';
    const W03 = 'W03';
    const W04 = 'W04';
    const W05 = 'W05';
    const W06 = 'W06';
    const W07 = 'W07';
    const W08 = 'W08';
    const W10 = 'W10';
    const W11 = 'W11';
    const W12 = 'W12';
    const W14 = 'W14';
    const W16 = 'W16';
    const W28 = 'W28';
    const W71 = 'W71';
    const W88 = 'W88';
    const E3 = 'E3';
    const E4 = 'E4';
    const E5 = 'E5';
    const E6 = 'E6';
    const E7 = 'E7';
    const E9 = 'E9';
    const E28 = 'E28';
    const E36 = 'E36';
    const PP05 = 'PP05';
    const PP09 = 'PP09';
    const PP12 = 'PP12';
    const PP14 = 'PP14';
    const PP20 = 'PP20';
    const PP26 = 'PP26';
    const PP28 = 'PP28';
    const PP77 = 'PP77';
    const PP88 = 'PP88';
    const PP92 = 'PP92';
    const PP96 = 'PP96';
}
