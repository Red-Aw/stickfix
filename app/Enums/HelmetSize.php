<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static XS()
 * @method static static S()
 * @method static static M()
 * @method static static L()
 * @method static static XL()
 *
 * @extends Enum<string>
 */
final class HelmetSize extends Enum
{
    const XS = 'XS';
    const S = 'S';
    const M = 'M';
    const L = 'L';
    const XL = 'XL';
}
