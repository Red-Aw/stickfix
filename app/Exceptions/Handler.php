<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

/**
 * Summary of Handler
 */
class Handler extends ExceptionHandler
{
    /**
     * Summary of dontReport
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [ ];

    /**
     * Summary of dontFlash
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Summary of register
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {

        });
    }
}
