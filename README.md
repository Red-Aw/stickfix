# StickFix

## Setup
1. Clone project: `git clone`
2. Install composer dependencies: `composer install`
3. Development environment (optional): `composer update --no-scripts`
4. Install NPM Dependencies: `npm install`
5. Create a copy of .env file: `cp .env.example .env` and add info
6. Generate an app encryption key: `php artisan key:generate`
7. Create an empty database for our application
8. Create migration: `php artisan make:migration create_example_table`
    8.1. Or if table exists then use: `php artisan make:migration add_column_to_example_table --table=example`
9. Manually run migrations that have not been executed yet: `php artisan migrate`
10. Seed the database: `php artisan db:seed`
11. Migrate fresh and seed together: `php artisan migrate:fresh --seed`
12. Launch code analyzer: `composer phpstan`

## Deployment

Steps:
1. add .env file. APP_ENV=production, APP_DEBUG=false, add correct URL, DB connection, mail settings, api keys.
2. run: `npm install` and after that `npm run build`. node_modules map not needed in prod
3. Follow [Laravel Deployment Steps](https://laravel.com/docs/10.x/deployment)
4. Follow [VueJs Deployment Steps](https://vuejs.org/guide/best-practices/production-deployment.html)

Optional steps:
1. in config/app.php change correct link and check settings
2. in config/mail.php check if correct settings are used 'from' and 'to' should be empty array. Default mailer is 'smtp'
3. in resources/js/components/Footer.vue add correct info

## Testing

Steps:
1. make sure .env.testing is set with testing db and configurations
2. switch to testing environment with command: `php artisan config:cache --env=testing`
3. seed db with default values: `php artisan migrate:fresh --seed`
4. run tests: `php artisan test`

## Testing stripe webhook

Steps:
1. Log in in stripe cli: `stripe login`
2. Forward events: `stripe listen --forward-to 127.0.0.1:8000/api/webhook`
3. Open 2nd console tab and trigger events: `stripe trigger checkout.session.completed`

## Cleaning project

Clear config and cache with commands:
- `php artisan cache:clear`
- `php artisan route:clear`
- `php artisan config:clear`
- `php artisan view:clear`
- `php artisan config:cache`

## Other commands:

Generate key:
- `php artisan key:generate`

Make controller:
- `php artisan make:controller NameController`
- `php artisan make:controller NameController --resource // adds crud methods`

Force run all migrations (refresh DB) and seed DB:
- `php artisan migrate:fresh --seed`

Manually run migrations that have not been executed yet and seed DB:
- `php artisan migrate`
- `php artisan db:seed`

Create Table:
- `php artisan make:migration create_new_table`

Create Model:
- `php artisan make:model Name -m`

Shows All Routes:
- `php artisan route:list`

Create Middleware:
- `php artisan make:middleware Name`
- After creating please register in: `app/Http/Kernel.php`

Create New Factory:
- `php artisan make:factory FactoryName`

Create New Test:
- `php artisan make:test UserTest --unit`

Run Tests:
- `php artisan test`

Switch environments:
- `php artisan config:cache --env=dev`
- `php artisan config:cache --env=prod`
- `php artisan config:cache --env=testing`

Laravel Version
- `php artisan --version`

Commands for development:
- `npm run dev`
- `php artisan serv`

See outdated packages:
- `npm outdated`
- `composer outdated`

Update Outdated packages:
- `npx npm-check-updates -u`
- `npm install`
- `composer update`

CLI:
- `php artisan tinker`

## MariaDB

PhpMyAdmin available via - [localhost/phpmyadmin](http://localhost/phpmyadmin)
Set env. variables:
- `DB_HOST=127.0.0.1`
- `DB_USERNAME=root`
- `DB_PASSWORD=`

## Technologies used

Built with VueJs 3, Vuetify 3, Laravel 10, Vite, MariaDB, Docker

## Docker

Uses Laravel Sail. To use sail use command: `./vendor/bin/sail {flag}` or via create alias via command: `alias sail='bash ./vendor/bin/sail'` and use shorthanded command: `sail {command}`

Few commands:
- Start containers: `sail up` in detached mode: `sail up -d`
- Stop containers: `sail down` or `sail stop`
- Rebuild images when updating docker-compose.yml: `sail build --no-cache`
- Container CLI: `sail shell` or `sail root-shell`. If there are permissions issues use shell to access container end execute chmod and chown commands: `chmod -R 755 ...` and `chown -R sail:sail ...`
- Run command for development: `sail npm run dev`
- Generate a key: `sail artisan key:generate`
- To run migrations: `sail artisan migrate` or fresh with seed `sail artisan migrate:fresh --seed`
- For development purposes PhpMyAdmin available via port 8081: `127.0.0.1:8081` for local development
- To use DB need to adjust env. variables: `DB_HOST=mariadb`, `DB_USERNAME=sail`, `DB_PASSWORD=password`