<?php

namespace Tests\Unit;

use App\Enums\Language;
use App\Http\Controllers\RolesController;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

/**
 * Summary of UserTest
 */
class UserTest extends TestCase
{
    /**
     * Summary of testGetUnauthenticatedUserData
     * @return void
     */
    public function testGetUnauthenticatedUserData()
    {
        $this->json('GET', 'api/user', ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);
    }

    /**
     * Summary of testGetAuthenticatedUserData
     * @return void
     */
    public function testGetAuthenticatedUserData()
    {
        $userData = [
            'username' => 'JohnDoe',
            'email' => 'doe@example.com',
            'password' => Hash::make('demo12345'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'doe@example.com',
            'password' => 'demo12345',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $this->json('GET', 'api/user', ['Accept' => 'application/json'])
            ->assertStatus(200);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testRegisterNewUserAsUnauthenticated
     * @return void
     */
    public function testRegisterNewUserAsUnauthenticated()
    {
        $newUserData = [
            'username' => 'JohnDoe',
            'email' => 'doe@example.com',
            'password' => 'demo12345',
            'isBlocked' => false,
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $newUserData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $newUserDataDb = [
            'username' => 'JohnDoe',
            'email' => 'doe@example.com',
            'password' => Hash::make('demo12345'),
            'isBlocked' => 0,
        ];

        $this->assertDatabaseMissing('users', $newUserDataDb);
    }

    /**
     * Summary of testRegisterNewUserAsAuthenticated
     * @return void
     */
    public function testRegisterNewUserAsAuthenticated()
    {
        $userData = [
            'username' => 'JohnDoe',
            'email' => 'doe@example.com',
            'password' => Hash::make('demo12345'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'doe@example.com',
            'password' => 'demo12345',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $newUserData = [
            'username' => 'DojesJon',
            'email' => 'doje123@example.com',
            'password' => 'test12345',
            'password_confirmation' => 'test12345',
            'language' => Language::LV,
            'isBlocked' => false,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeUser', $newUserData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $newUserDataDb = [
            'username' => 'DojesJon',
        ];

        $this->assertDatabaseMissing('users', $newUserDataDb);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::ADD_NEW_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('POST', 'api/storeUser', $newUserData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseHas('users', $newUserDataDb);

        User::where('username', $newUserDataDb['username'])->delete();

        $this->assertDatabaseMissing('users', $newUserDataDb);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAlreadyTakenEmailAsAuthenticated
     * @return void
     */
    public function testAlreadyTakenEmailAsAuthenticated()
    {
        $userData = [
            'username' => 'RandomUser',
            'email' => 'randomuser@example.com',
            'password' => Hash::make('randompassword'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'randomuser@example.com',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $data = [
            'email' => 'randomuser@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'available' => false,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAlreadyTakenEmailAsUnauthenticated
     * @return void
     */
    public function testAlreadyTakenEmailAsUnauthenticated()
    {
        $userData = [
            'username' => 'RandomUserx',
            'email' => 'randomuserx@example.com',
            'password' => Hash::make('randompassword'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'randomuserx@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAvailableEmailAsAuthenticated
     * @return void
     */
    public function testAvailableEmailAsAuthenticated()
    {
        $userData = [
            'username' => 'RandomUserz',
            'email' => 'randomuserz@example.com',
            'password' => Hash::make('randompassword'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'randomuserz@example.com',
            'password' => 'randompassword',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $data = [
            'email' => 'availableemail@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'available' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAvailableEmailAsUnauthenticated
     * @return void
     */
    public function testAvailableEmailAsUnauthenticated()
    {
        $userData = [
            'username' => 'RandomUsery',
            'email' => 'randomusery@example.com',
            'password' => Hash::make('randompassword'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'availableemail2@example.com',
        ];

        $this->json('POST', 'api/checkEmailAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAlreadyTakenUsernameAsAuthenticated
     * @return void
     */
    public function testAlreadyTakenUsernameAsAuthenticated()
    {
        $userData = [
            'username' => 'gwendolyn',
            'email' => 'gwendolyn.bahringer6@gmail.com',
            'password' => Hash::make('gwendolyn123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'gwendolyn.bahringer6@gmail.com',
            'password' => 'gwendolyn123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $data = [
            'username' => 'gwendolyn',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'available' => false,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAlreadyTakenUsernameAsUnauthenticated
     * @return void
     */
    public function testAlreadyTakenUsernameAsUnauthenticated()
    {
        $userData = [
            'username' => 'florencio_haley',
            'email' => 'florencio_haley@hotmail.com',
            'password' => Hash::make('florencio_haley123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'username' => 'florencio_haley',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAvailableUsernameAsAuthenticated
     * @return void
     */
    public function testAvailableUsernameAsAuthenticated()
    {
        $userData = [
            'username' => 'marion_padberg',
            'email' => 'marion_padberg@yahoo.com',
            'password' => Hash::make('marion_padberg123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'marion_padberg@yahoo.com',
            'password' => 'marion_padberg123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $data = [
            'username' => 'marion_padberg1',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'available' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testAvailableUsernameAsUnauthenticated
     * @return void
     */
    public function testAvailableUsernameAsUnauthenticated()
    {
        $userData = [
            'username' => 'lurline55',
            'email' => 'lurline55@yahoo.com',
            'password' => Hash::make('lurline55123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'username' => 'lurline551',
        ];

        $this->json('POST', 'api/checkUsernameAvailability', $data, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testLoginAndLogoutAsVerifiedUser
     * @return void
     */
    public function testLoginAndLogoutAsVerifiedUser()
    {
        $userData = [
            'username' => 'barrett_stroman71',
            'email' => 'barrett_stroman71@yahoo.com',
            'password' => Hash::make('barrett_stroman71123'),
            'isConfirmed' => 1,
            'isBlocked' => 0,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'barrett_stroman71@yahoo.com',
            'password' => 'barrett_stroman71123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testLoginAndLogoutAsNotExistingUser
     * @return void
     */
    public function testLoginAndLogoutAsNotExistingUser()
    {
        $userData = [
            'username' => 'beau.wiza',
            'email' => 'beau.wiza@hotmail.com',
            'password' => Hash::make('beau.wiza123'),
            'isConfirmed' => 1,
        ];

        $this->assertDatabaseMissing('users', $userData);

        $data = [
            'email' => 'beau.wiza@hotmail.com',
            'password' => 'beau.wiza123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }

    /**
     * Summary of testLoginAsUnverifiedUser
     * @return void
     */
    public function testLoginAsUnverifiedUser()
    {
        $userData = [
            'username' => 'gianni_barton86',
            'email' => 'gianni_barton86@hotmail.com',
            'password' => Hash::make('gianni_barton8645'),
            'isConfirmed' => 0,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'gianni_barton86@hotmail.com',
            'password' => 'gianni_barton8645',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testLoginAsUnverifiedAndBlockedUser
     * @return void
     */
    public function testLoginAsUnverifiedAndBlockedUser()
    {
        $userData = [
            'username' => 'aida_mcdermott19',
            'email' => 'aida_mcdermott19@gmail.com',
            'password' => Hash::make('aida_mcdermott194g'),
            'isConfirmed' => 0,
            'isBlocked' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'aida_mcdermott19@gmail.com',
            'password' => 'aida_mcdermott194g',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testLoginAsBlockedUser
     * @return void
     */
    public function testLoginAsBlockedUser()
    {
        $userData = [
            'username' => 'gabriellaroberts',
            'email' => 'gabriella.roberts@gmail.com',
            'password' => Hash::make('gabr4ielladds'),
            'isConfirmed' => 1,
            'isBlocked' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'gabriella.roberts@gmail.com',
            'password' => 'gabr4ielladds',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testConfirmUserAsUnauthenticated
     * @return void
     */
    public function testConfirmUserAsUnauthenticated()
    {
        $userData = [
            'username' => 'isac.ebert',
            'email' => 'isac.ebert@yahoo.com',
            'password' => Hash::make('isac.343aebert'),
            'isConfirmed' => 0,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/confirmUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testConfirmUserAsAuthenticated
     * @return void
     */
    public function testConfirmUserAsAuthenticated()
    {
        $userData = [
            'username' => 'alford45',
            'email' => 'alford45@gmail.com',
            'password' => Hash::make('alford45axd3'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'alford45@gmail.com',
            'password' => 'alford45axd3',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/confirmUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::CONFIRM_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('GET', 'api/confirmUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $secondUserData = [
            'username' => 'branson_wunsch4',
            'email' => 'branson_wunsch4@yahoo.com',
            'password' => Hash::make('branson_wunsch4as'),
            'isConfirmed' => 0,
        ];

        $secondUser = User::factory()->create($secondUserData);

        $this->assertDatabaseHas('users', $secondUserData);

        $this->json('GET', 'api/confirmUser/' . $secondUser->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $secondUserData['isConfirmed'] = 1;

        $this->assertDatabaseHas('users', $secondUserData);

        $this->json('GET', 'api/confirmUser/' . $secondUser->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $secondUserData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);

        $secondUser->delete();
        $this->assertDatabaseMissing('users', $secondUserData);
    }

    /**
     * Summary of testConfirmNonExistingUserAsAuthenticated
     * @return void
     */
    public function testConfirmNonExistingUserAsAuthenticated()
    {
        $userData = [
            'username' => 'brian31',
            'email' => 'brian31@hotmail.com',
            'password' => Hash::make('brian31apan'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'brian31@hotmail.com',
            'password' => 'brian31apan',
            'recaptcha' => 'testing',
        ];

        $this->assertDatabaseHas('users', $userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::CONFIRM_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/confirmUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSendVerificationAsUnauthenticated
     * @return void
     */
    public function testSendVerificationAsUnauthenticated()
    {
        $userData = [
            'username' => 'quinn.walkert',
            'email' => 'quinn.walkert@hotmail.com',
            'password' => Hash::make('walker2ewq'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/resendVerification/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSendVerificationAsAuthenticated
     * @return void
     */
    public function testSendVerificationAsAuthenticated()
    {
        $userData = [
            'username' => 'effie73',
            'email' => 'effie73@gmail.com',
            'password' => Hash::make('effie735gsd'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'effie73@gmail.com',
            'password' => 'effie735gsd',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/resendVerification/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::CONFIRM_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('GET', 'api/resendVerification/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $secondUserData = [
            'username' => 'jan.murazi',
            'email' => 'jan.murazi@hotmail.com',
            'password' => Hash::make('jan.murazias3da'),
            'isConfirmed' => 0,
        ];

        $secondUser = User::factory()->create($secondUserData);

        $this->assertDatabaseHas('users', $secondUserData);

        $this->json('GET', 'api/resendVerification/' . $secondUser->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseHas('users', $secondUserData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);

        $secondUser->delete();
        $this->assertDatabaseMissing('users', $secondUserData);
    }

    /**
     * Summary of testSendVerificationAsAuthenticated
     * @return void
     */
    public function testIfVerificationViewIsShownToUnverifiedUser()
    {
        $userData = [
            'username' => 'brookerogahn',
            'email' => 'brooke_rogahn@hotmail.com',
            'password' => Hash::make('brookerogahn3tsd'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/checkUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->json('GET', 'api/setVerificationAsShown/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/checkUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSendVerificationAsAuthenticated
     * @return void
     */
    public function testIfVerificationViewIsShownToVerifiedUser()
    {
        $userData = [
            'username' => 'agustina44',
            'email' => 'agustina44@gmail.com',
            'password' => Hash::make('agustina4asd5'),
            'isConfirmed' => 1,
            'isShownToUser' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/checkUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSendVerificationForNonExistingUserAsAuthenticated
     * @return void
     */
    public function testSendVerificationForNonExistingUserAsAuthenticated()
    {
        $userData = [
            'username' => 'philip_johnson',
            'email' => 'philip_johnson@yahoo.com',
            'password' => Hash::make('philip_johnsonas3d'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'philip_johnson@yahoo.com',
            'password' => 'philip_johnsonas3d',
            'recaptcha' => 'testing',
        ];

        $this->assertDatabaseHas('users', $userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::CONFIRM_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/resendVerification/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testCheckOnAuthorizationAsAuthenticatedUser
     * @return void
     */
    public function testCheckOnAuthorizationAsAuthenticatedUser()
    {
        $userData = [
            'username' => 'glenda_jones72',
            'email' => 'glenda_jones72@hotmail.com',
            'password' => Hash::make('glenda_jones72123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'glenda_jones72@hotmail.com',
            'password' => 'glenda_jones72123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/checkAuth', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testCheckOnAuthorizationAsUnauthenticatedUser
     * @return void
     */
    public function testCheckOnAuthorizationAsUnauthenticatedUser()
    {
        $this->json('GET', 'api/checkAuth', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);
    }

    /**
     * Summary of testCheckOnAuthorizationAsAuthenticatedUserWithSuperuserRole
     * @return void
     */
    public function testCheckOnAuthorizationAsAuthenticatedUserWithSuperuserRole()
    {
        $userData = [
            'username' => 'amy16',
            'email' => 'amy16@hotmail.com',
            'password' => Hash::make('amy16123$kv'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::SUPER_ADMIN,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('users', $userData);
        $this->assertDatabaseHas('roles', $roleData);

        $data = [
            'email' => 'amy16@hotmail.com',
            'password' => 'amy16123$kv',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        foreach (RolesController::ROLES as $roleName) {
            $this->json('GET', 'api/checkAuth/' . $roleName, ['Accept' => 'application/json'])
                ->assertStatus(200)
                ->assertJson([
                    'success' => true,
                ]);
        }

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testCheckOnAuthorizationAsAuthenticatedUserWithAddNewUserRole
     * @return void
     */
    public function testCheckOnAuthorizationAsAuthenticatedUserWithAddNewUserRole()
    {
        $userData = [
            'username' => 'landen_olson',
            'email' => 'landen_olson@gmail.com',
            'password' => Hash::make('landen_olson123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::ADD_NEW_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('users', $userData);
        $this->assertDatabaseHas('roles', $roleData);

        $data = [
            'email' => 'landen_olson@gmail.com',
            'password' => 'landen_olson123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        foreach (RolesController::ROLES as $roleName) {
            if ($roleName === RolesController::ADD_NEW_USER) {
                $this->json('GET', 'api/checkAuth/' . $roleName, ['Accept' => 'application/json'])
                    ->assertStatus(200)
                    ->assertJson([
                        'success' => true,
                    ]);
            } else {
                $this->json('GET', 'api/checkAuth/' . $roleName, ['Accept' => 'application/json'])
                    ->assertStatus(200)
                    ->assertJson([
                        'success' => false,
                    ]);
            }
        }

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testCheckOnAuthorizationAsAuthenticatedUserWithNoRoles
     * @return void
     */
    public function testCheckOnAuthorizationAsAuthenticatedUserWithNoRoles()
    {
        $userData = [
            'username' => 'pietro.parisian',
            'email' => 'pietro.parisian64@hotmail.com',
            'password' => Hash::make('pietro.parisian123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'pietro.parisian64@hotmail.com',
            'password' => 'pietro.parisian123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        foreach (RolesController::ROLES as $roleName) {
            $this->json('GET', 'api/checkAuth/' . $roleName, ['Accept' => 'application/json'])
                ->assertStatus(200)
                ->assertJson([
                    'success' => false,
                ]);
        }

        $this->json('GET', 'api/logout', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testChangeUserPassword
     * @return void
     */
    public function testChangeUserPassword()
    {
        $userData = [
            'username' => 'quentin.rogahn26',
            'email' => 'quentin.rogahn26@hotmail.com',
            'password' => Hash::make('quentin.rogahn26123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'quentin.rogahn26@hotmail.com',
            'password' => 'quentin.rogahn26123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $newPasswordData = [
            'id' => $user->id,
            'currentPassword' => 'quentin.rogahn26123',
            'newPassword' => 'gwendolyn123',
            'newPassword_confirmation' => 'gwendolyn123',
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make('gwendolyn123');

        $this->json('POST', 'api/newPass', $newPasswordData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $newLoginData = [
            'email' => 'quentin.rogahn26@hotmail.com',
            'password' => 'gwendolyn123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $newLoginData, ['Accept' => 'application/json']);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testCheckPasswordsMatch
     * @return void
     */
    public function testCheckPasswordsMatch()
    {
        $userData = [
            'username' => 'qoberbrunner41',
            'email' => 'zora.oberbrunner41@yahoo.com',
            'password' => Hash::make('oberbrunner41123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'zora.oberbrunner41@yahoo.com',
            'password' => 'oberbrunner41123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $passwordData = [
            'id' => $user->id,
            'currentPassword' => 'oberbrunner41123',
        ];

        $this->json('POST', 'api/checkPasswordMatch', $passwordData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'match' => true,
            ]);

        $passwordData = [
            'id' => $user->id,
            'currentPassword' => 'oberbrunner41yxz',
        ];

        $this->json('POST', 'api/checkPasswordMatch', $passwordData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'match' => false,
            ]);


        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetUserRoles
     * @return void
     */
    public function testGetUserRoles()
    {
        $userData = [
            'username' => 'michel36',
            'email' => 'michel36@gmail.com',
            'password' => Hash::make('michel36123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $userRoles = [
            [
                'userId' => $user->id,
                'role' => RolesController::READ_MESSAGES,
            ],
            [
                'userId' => $user->id,
                'role' => RolesController::MARK_USER,
            ],
        ];

        $createdRoles = [];
        foreach ($userRoles as $userRole) {
            $createdRoles[] = Role::factory()->create($userRole);
        }

        $this->assertDatabaseHas('users', $userData);
        foreach ($userRoles as $userRole) {
            $this->assertDatabaseHas('roles', $userRole);
        }

        $data = [
            'email' => 'michel36@gmail.com',
            'password' => 'michel36123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/getRoles', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'roles' => [],
            ])
            ->assertJsonCount(2, 'roles');

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        foreach ($createdRoles as $createdRole) {
            $createdRole->delete();
        }

        foreach ($userRoles as $userRole) {
            $this->assertDatabaseMissing('roles', $userRole);
        }

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetAllRoles
     * @return void
     */
    public function testGetAllRoles()
    {
        $userData = [
            'username' => 'katlyn30',
            'email' => 'katlyn30@hotmail.com',
            'password' => Hash::make('katlyn30123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $data = [
            'email' => 'katlyn30@hotmail.com',
            'password' => 'katlyn30123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/getAllRoles', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson(
                RolesController::ROLES,
            );

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testEditUserData
     * @return void
     */
    public function testEditUserData()
    {
        $userData = [
            'username' => 'elijah35',
            'email' => 'elijah35@hotmail.com',
            'password' => Hash::make('elijah35123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'elijah35@hotmail.com',
            'password' => 'elijah35123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $newUserData = [
            'id' => $user->id,
            'username' => 'frida_heaney67',
            'originalUsername' => 'elijah35',
            'email' => 'frida_heaney67@yahoo.com',
            'originalEmail' => 'elijah35@hotmail.com',
            'language' => Language::LV,
            'isBlocked' => false,
            'roles' => [],
            'recaptcha' => 'testing',
        ];

        $userData['username'] = 'frida_heaney67';
        $userData['email'] = 'frida_heaney67@yahoo.com';

        $this->json('POST', 'api/editUser', $newUserData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseMissing('users', $userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::EDIT_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('POST', 'api/editUser', $newUserData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testMarkUser
     * @return void
     */
    public function testMarkUser()
    {
        $userData = [
            'username' => 'stephany93',
            'email' => 'stephany93@gmail.com',
            'password' => Hash::make('stephany93123'),
            'isConfirmed' => 1,
            'isBlocked' => 0,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'stephany93@gmail.com',
            'password' => 'stephany93123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/markUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::MARK_USER,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('GET', 'api/markUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('users', $userData);

        $secondUserData = [
            'username' => 'jo_morissette',
            'email' => 'jo_morissette@yahoo.com',
            'password' => Hash::make('jo_morissette23d'),
            'isConfirmed' => 1,
            'isBlocked' => 0,
        ];

        $secondUser = User::factory()->create($secondUserData);

        $this->assertDatabaseHas('users', $secondUserData);

        $this->json('GET', 'api/markUser/' . $secondUser->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $secondUserData['isBlocked'] = 1;

        $this->assertDatabaseHas('users', $secondUserData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);

        $secondUser->delete();
        $this->assertDatabaseMissing('users', $secondUserData);
    }

    /**
     * Summary of testGetUser
     * @return void
     */
    public function testGetUser()
    {
        $userData = [
            'username' => 'ansel.hoeger1',
            'email' => 'ansel.hoeger18@yahoo.com',
            'password' => Hash::make('ansel.hoeger1123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'ansel.hoeger18@yahoo.com',
            'password' => 'ansel.hoeger1123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/getUser/' . $user->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'user' => [ 'id' => $user->id ],
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetUsers
     * @return void
     */
    public function testGetUsers()
    {
        $this->truncateUserTable();

        $userData = [
            'username' => 'virginia.franecki23',
            'email' => 'virginia.franecki23@yahoo.com',
            'password' => Hash::make('virginia.franecki23123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'virginia.franecki23@yahoo.com',
            'password' => 'virginia.franecki23123',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/getUsers', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'users' => [
                    [ 'id' => 1 ],
                    [ 'id' => $user->id ],
                ],
            ])
            ->assertJsonCount(2, 'users'); // 2 because of default user

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSwitchShowInactiveAsUnauthenticated
     * @return void
     */
    public function testSwitchShowInactiveAsUnauthenticated()
    {
        $userData = [
            'username' => 'melany32',
            'email' => 'melany32@gmail.com',
            'password' => Hash::make('melany328aj'),
            'isConfirmed' => true,
            'isBlocked' => false,
            'showInactive' => true,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/switchShowInactive', ['Accept' => 'application/json'])
            ->assertStatus(401);

        $this->assertDatabaseHas('users', $userData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSwitchShowInactiveAsAuthenticated
     * @return void
     */
    public function testSwitchShowInactiveAsAuthenticated()
    {
        $userData = [
            'username' => 'bertha.boyle40',
            'email' => 'bertha.boyle40@gmail.com',
            'password' => Hash::make('bertha.boyle4012da'),
            'isConfirmed' => true,
            'isBlocked' => false,
            'showInactive' => true,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'bertha.boyle40@gmail.com',
            'password' => 'bertha.boyle4012da',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/switchShowInactive', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('users', $userData);

        $userData['showInactive'] = false;

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/switchShowInactive', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('users', $userData);

        $userData['showInactive'] = true;

        $this->assertDatabaseHas('users', $userData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSwitchShowDeletedAsUnauthenticated
     * @return void
     */
    public function testSwitchShowDeletedAsUnauthenticated()
    {
        $userData = [
            'username' => 'prudence_mayer30',
            'email' => 'prudence_mayer30@gmail.com',
            'password' => Hash::make('prudence_mayer30sd1'),
            'isConfirmed' => true,
            'isBlocked' => false,
            'showInactive' => true,
            'showDeleted' => true,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/switchShowDeleted', ['Accept' => 'application/json'])
            ->assertStatus(401);

        $this->assertDatabaseHas('users', $userData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testSwitchShowDeletedAsAuthenticated
     * @return void
     */
    public function testSwitchShowDeletedAsAuthenticated()
    {
        $userData = [
            'username' => 'senger80',
            'email' => 'senger80@yahoo.com',
            'password' => Hash::make('senger801sds'),
            'isConfirmed' => true,
            'isBlocked' => false,
            'showInactive' => true,
            'showDeleted' => true,
        ];

        $user = User::factory()->create($userData);

        $this->assertDatabaseHas('users', $userData);

        $loginData = [
            'email' => 'senger80@yahoo.com',
            'password' => 'senger801sds',
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/switchShowDeleted', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('users', $userData);

        $userData['showDeleted'] = false;

        $this->assertDatabaseHas('users', $userData);

        $this->json('GET', 'api/switchShowDeleted', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('users', $userData);

        $userData['showDeleted'] = true;

        $this->assertDatabaseHas('users', $userData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testClearUserAndRoleTable
     * @return void
     */
    public function testClearUserAndRoleTable()
    {
        $this->truncateUserTable();

        $this->assertDatabaseCount('users', 1);
        $this->assertDatabaseCount('roles', 1);
    }

    /**
     * Summary of truncateUserTable
     * @return void
     */
    private function truncateUserTable()
    {
        if (config('app.env') === 'testing') {
            User::where('id', '<>', 1)->delete();
            Role::where('userId', '<>', 1)->delete();
        }
    }
}
