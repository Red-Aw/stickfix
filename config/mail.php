<?php

return [

    'default' => env('MAIL_MAILER'),

    'mailers' => [
        'default' => [
            'transport' => 'smtp',
            'host' => env('MAIL_HOST'),
            'port' => env('MAIL_PORT'),
            'encryption' => env('MAIL_ENCRYPTION'),
            'username' => env('MAIL_USERNAME'),
            'password' => env('MAIL_PASSWORD'),
            'timeout' => null,
            'auth_mode' => null,
        ],

        'contact' => [
            'transport' => 'smtp',
            'host' => env('MAIL_HOST'),
            'port' => env('MAIL_PORT'),
            'encryption' => env('MAIL_ENCRYPTION'),
            'username' => env('CONTACT_MAIL_USERNAME'),
            'password' => env('CONTACT_MAIL_PASSWORD'),
            'timeout' => null,
            'auth_mode' => null,
        ],

        'noreply' => [
            'transport' => 'smtp',
            'host' => env('MAIL_HOST'),
            'port' => env('MAIL_PORT'),
            'encryption' => env('MAIL_ENCRYPTION'),
            'username' => env('NOREPLY_MAIL_USERNAME'),
            'password' => env('NOREPLY_MAIL_PASSWORD'),
            'timeout' => null,
            'auth_mode' => null,
        ],

        'info' => [
            'transport' => 'smtp',
            'host' => env('MAIL_HOST'),
            'port' => env('MAIL_PORT'),
            'encryption' => env('MAIL_ENCRYPTION'),
            'username' => env('INFO_MAIL_USERNAME'),
            'password' => env('INFO_MAIL_PASSWORD'),
            'timeout' => null,
            'auth_mode' => null,
        ],

        'ses' => [
            'transport' => 'ses',
        ],

        'mailgun' => [
            'transport' => 'mailgun',
        ],

        'postmark' => [
            'transport' => 'postmark',
        ],

        'sendmail' => [
            'transport' => 'sendmail',
            'path' => '/usr/sbin/sendmail -bs',
        ],

        'log' => [
            'transport' => 'log',
            'channel' => env('MAIL_LOG_CHANNEL'),
        ],

        'array' => [
            'transport' => 'array',
        ],
    ],

    'contact' => [
        'address' => env('CONTACT_MAIL_USERNAME'),
        'name' => env('MAIL_FROM_NAME')
    ],

    'noreply' => [
        'address' => env('NOREPLY_MAIL_USERNAME'),
        'name' => env('MAIL_FROM_NAME')
    ],

    'info' => [
        'address' => env('INFO_MAIL_USERNAME'),
        'name' => env('MAIL_FROM_NAME')
    ],

    'notification' => [
        'address' => env('NOTIFICATION_ADDRESS'),
        'name' => null
    ],

    'from' => [ ],

    'to' => [ ],

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

];
