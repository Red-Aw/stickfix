export default {
    namespaced: true,

    state: {
        cart: [],
        isLoading: true,
    },

    getters: {
        cart (state) {
            return state.cart
        },
        isLoading (state) {
            return state.isLoading
        },
    },

    mutations: {
        SET_CART (state, value) {
            state.cart = value
        },
        SET_IS_LOADING (state, value) {
            state.isLoading = value
        },
    },

    actions: {
        async getCartContent ({ dispatch }) {
            return dispatch('getCart')
        },

        async increaseQuantityOfItemOrAddItemToCart ({ dispatch }, payload) {
            return dispatch('increaseQuantityOrAddToCart', payload)
        },

        async decreaseQuantityOfItemOrRemoveItemFromCart ({ dispatch }, payload) {
            return dispatch('decreaseQuantityOrRemoveFromCart', payload)
        },

        async removeItemsFromCart ({ dispatch }, payload) {
            return dispatch('removeFromCart', payload)
        },

        async emptyTheCart ({ dispatch }) {
            return dispatch('removeAllItemsFromCart')
        },

        getCart ({ commit }) {
            try {
                commit('SET_IS_LOADING', true);

                const cartProducts = helperMethods.getProductListWithFilteringDuplicates();

                commit('SET_CART', cartProducts);
                commit('SET_IS_LOADING', false);
            } catch(error) {
                console.error(error);
                commit('SET_CART', []);
                commit('SET_IS_LOADING', true);
            }
        },

        increaseQuantityOrAddToCart ({ commit }, payload) {
            try {
                commit('SET_IS_LOADING', true);

                let cartProducts = helperMethods.getProductList();
                const productIndex = helperMethods.findProductIndexInCart(cartProducts, payload.itemId)
                if (productIndex === -1) {
                    cartProducts.push({ id: payload.itemId, quantity: payload.quantity });
                } else {
                    cartProducts[productIndex].quantity = cartProducts[productIndex].quantity + payload.quantity;
                }

                cartProducts = helperMethods.filterDuplicates(cartProducts);
                localStorage.setItem('cart', JSON.stringify(cartProducts));

                commit('SET_CART', cartProducts);
                commit('SET_IS_LOADING', false);
            } catch(error) {
                console.error(error);
                commit('SET_CART', []);
                commit('SET_IS_LOADING', true);
            }
        },

        decreaseQuantityOrRemoveFromCart ({ commit }, payload) {
            try {
                commit('SET_IS_LOADING', true);

                let cartProducts = helperMethods.getProductList();
                const productIndex = helperMethods.findProductIndexInCart(cartProducts, payload.itemId);
                if (productIndex !== -1) {
                    cartProducts[productIndex].quantity = cartProducts[productIndex].quantity - payload.quantity;
                    if (cartProducts[productIndex].quantity <= 0) {
                        cartProducts = cartProducts.filter((product) => product.id !== payload.itemId);
                    }
                }

                localStorage.setItem('cart', JSON.stringify(cartProducts));

                commit('SET_CART', cartProducts);
                commit('SET_IS_LOADING', false);
            } catch(error) {
                console.error(error);
                commit('SET_CART', []);
                commit('SET_IS_LOADING', true);
            }
        },

        removeFromCart ({ commit }, payload) {
            try {
                commit('SET_IS_LOADING', true);

                let cartProducts = helperMethods.getProductList();
                cartProducts = cartProducts.filter((product) => {
                    return !payload.items.some((item) => item.id === product.id);
                });
                localStorage.setItem('cart', JSON.stringify(cartProducts));

                commit('SET_CART', cartProducts);
                commit('SET_IS_LOADING', false);
            } catch(error) {
                console.error(error);
                commit('SET_CART', []);
                commit('SET_IS_LOADING', true);
            }
        },

        removeAllItemsFromCart ({ commit }) {
            try {
                commit('SET_IS_LOADING', true);

                const cartProducts = [];
                localStorage.setItem('cart', JSON.stringify(cartProducts));

                commit('SET_CART', cartProducts);
                commit('SET_IS_LOADING', false);
            } catch(error) {
                console.error(error);
                commit('SET_CART', []);
                commit('SET_IS_LOADING', true);
            }
        },
    },
}

const helperMethods = {
    getProductList () {
        let productList = [];
        if (localStorage.getItem('cart')) {
            productList = JSON.parse(localStorage.getItem('cart'));
        }
        if (!Array.isArray(productList)) {
            localStorage.setItem('cart', JSON.stringify([]));
            productList = [];
        }
        return productList;
    },

    getProductListWithFilteringDuplicates () {
        let productList = [];
        if (localStorage.getItem('cart')) {
            productList = JSON.parse(localStorage.getItem('cart'));
            productList = this.filterDuplicates(productList);
        } else {
            localStorage.setItem('cart', JSON.stringify(productList))
        }
        return productList;
    },

    filterDuplicates (productList) {
        if (productList.length > 1) {
            return productList.filter((v, i, a) => a.findIndex(v2 => (v2.id === v.id)) === i);
        }
        return productList;
    },

    findProductIndexInCart (productList, itemId) {
        if (!Array.isArray(productList) || productList.length === 0) {
            return -1;
        }

        return productList.findIndex((item) => {
            if (!item.id) {
                return -1;
            }
            return item.id === itemId
        });
    },
};
