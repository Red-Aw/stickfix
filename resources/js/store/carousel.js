export default {
    namespaced: true,

    state: {
        carouselList: null,
        isLoading: true,
    },

    getters: {
        carouselList (state) {
            return state.carouselList
        },
        isLoading (state) {
            return state.isLoading
        },
    },

    mutations: {
        SET_CAROUSEL_LIST (state, value) {
            state.carouselList = value
        },
        SET_IS_LOADING (state, value) {
            state.isLoading = value
        },
    },

    actions: {
        async getCarouselItems ({ dispatch }) {
            return dispatch('getCarouselList')
        },

        getCarouselList ({ commit }) {
            commit('SET_IS_LOADING', true);
            return axios.get('/api/getCarouselItems').then(response => {
                if (response.data.success) {
                    commit('SET_CAROUSEL_LIST', response.data.items);
                    commit('SET_IS_LOADING', false);
                } else {
                    commit('SET_CAROUSEL_LIST', []);
                    commit('SET_IS_LOADING', true);
                }
            }).catch((error) => {
                console.error(error);
                commit('SET_CAROUSEL_LIST', []);
                commit('SET_IS_LOADING', true);
            });
        }
    }
}
