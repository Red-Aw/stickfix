export default {
    namespaced: true,

    state: {
        customerData: null,
    },

    getters: {
        customerData (state) {
            return state.customerData
        },
    },

    mutations: {
        SET_CUSTOMER_DATA (state, value) {
            state.customerData = value
        },
    },

    actions: {
        async getCustomerData ({ dispatch }) {
            return dispatch('getCustomer');
        },

        async saveCustomerData ({ dispatch }, payload) {
            return dispatch('setCustomer', payload);
        },

        getCustomer ({ commit }) {
            try {
                commit('SET_CUSTOMER_DATA', helperMethods.getCustomer());
            } catch(error) {
                console.error(error);
                commit('SET_CUSTOMER_DATA', null);
            }
        },

        setCustomer ({ commit }, payload) {
            try {
                localStorage.setItem('customer', JSON.stringify(payload));
                commit('SET_CUSTOMER_DATA', payload);
            } catch(error) {
                console.error(error);
                commit('SET_CUSTOMER_DATA', null);
            }
        },
    },
}

const helperMethods = {
    getCustomer () {
        if (localStorage.getItem('customer')) {
            return JSON.parse(localStorage.getItem('customer'));
        }
        return null;
    },
};
