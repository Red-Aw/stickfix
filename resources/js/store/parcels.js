export default {
    namespaced: true,

    state: {
        parcelMachineList: null,
        isLoading: true,
    },

    getters: {
        parcelMachineList (state) {
            return state.parcelMachineList
        },
        isLoading (state) {
            return state.isLoading
        },
    },

    mutations: {
        SET_PARCEL_MACHINE_LIST (state, value) {
            state.parcelMachineList = value
        },
        SET_IS_LOADING (state, value) {
            state.isLoading = value
        },
    },

    actions: {
        async getParcelMachineList ({ dispatch }) {
            return dispatch('getParcelMachineLocations')
        },

        getParcelMachineLocations ({ commit }) {
            const LOCATIONS_URL = 'https://www.omniva.lv/locations.json';

            // Remove because we get CORS error
            delete axios.defaults.headers.common['X-Requested-With'];
            axios.defaults.withCredentials = false;

            commit('SET_IS_LOADING', true);
            return axios(LOCATIONS_URL).then((response) => {
                // Set headers back up
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.defaults.withCredentials = true;

                if (response.data && response.data.length > 0) {
                    commit('SET_PARCEL_MACHINE_LIST', response.data);
                    commit('SET_IS_LOADING', false);
                } else {
                    commit('SET_PARCEL_MACHINE_LIST', []);
                    commit('SET_IS_LOADING', true);
                }
            }).catch((error) => {
                // Set headers back up
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

                console.error(error);
                commit('SET_PARCEL_MACHINE_LIST', []);
                commit('SET_IS_LOADING', true);
            });
        }
    }
}
