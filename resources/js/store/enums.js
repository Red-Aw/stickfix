export default {
    namespaced: true,

    state: {
        categories: [],
        conditions: [],
        messageOptions: [],
        serviceTypes: [],
        brands: [],
        sizes: [],
        languages: [],
        genders: [],
        countryCodes: [],
        deliveryTypes: [],
        orderStatus: [],
    },

    getters: {
        categories (state) {
            return state.categories;
        },
        conditions (state) {
            return state.conditions;
        },
        messageOptions (state) {
            return state.messageOptions;
        },
        serviceTypes (state) {
            return state.serviceTypes;
        },
        brands (state) {
            return state.brands;
        },
        sizes (state) {
            return state.sizes;
        },
        languages (state) {
            return state.languages;
        },
        genders (state) {
            return state.genders;
        },
        countryCodes (state) {
            return state.countryCodes;
        },
        deliveryTypes (state) {
            return state.deliveryTypes;
        },
        orderStatus (state) {
            return state.orderStatus;
        },
    },

    mutations: {
        SET_CATEGORIES (state, value) {
            state.categories = value
        },
        SET_CONDITIONS (state, value) {
            state.conditions = value
        },
        SET_MESSAGE_OPTIONS (state, value) {
            state.messageOptions = value
        },
        SET_SERVICE_TYPES (state, value) {
            state.serviceTypes = value
        },
        SET_BRANDS (state, value) {
            state.brands = value
        },
        SET_SIZES (state, value) {
            state.sizes = value
        },
        SET_LANGUAGES (state, value) {
            state.languages = value
        },
        SET_GENDERS (state, value) {
            state.genders = value
        },
        SET_COUNTRY_CODES (state, value) {
            state.countryCodes = value
        },
        SET_DELIVERY_TYPES (state, value) {
            state.deliveryTypes = value
        },
        SET_ORDER_STATUS (state, value) {
            state.orderStatus = value
        },
    },

    actions: {
        getAllEnums({ commit }) {
            return axios.get('/api/getAllEnums').then(response => {
                commit('SET_CATEGORIES', response.data.CATEGORIES);
                commit('SET_CONDITIONS', response.data.CONDITIONS);
                commit('SET_MESSAGE_OPTIONS', response.data.MESSAGE_OPTIONS);
                commit('SET_SERVICE_TYPES', response.data.SERVICE_TYPES);
                commit('SET_BRANDS', response.data.BRANDS);
                commit('SET_SIZES', response.data.SIZES.original);
                commit('SET_LANGUAGES', response.data.LANGUAGES);
                commit('SET_GENDERS', response.data.GENDERS);
                commit('SET_COUNTRY_CODES', response.data.COUNTRY_CODES);
                commit('SET_DELIVERY_TYPES', response.data.DELIVERY_TYPES);
            }).catch((error) => {
                console.error(error);
                commit('SET_CATEGORIES', []);
                commit('SET_CONDITIONS', []);
                commit('SET_MESSAGE_OPTIONS', []);
                commit('SET_SERVICE_TYPES', []);
                commit('SET_BRANDS', []);
                commit('SET_SIZES', []);
                commit('SET_LANGUAGES', []);
                commit('SET_GENDERS', []);
                commit('SET_COUNTRY_CODES', []);
                commit('SET_DELIVERY_TYPES', []);
            });
        },

        getCategories({ commit }) {
            return axios.get('/api/getCategories').then(response => {
                commit('SET_CATEGORIES', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_CATEGORIES', []);
            });
        },

        getConditions({ commit }) {
            return axios.get('/api/getConditions').then(response => {
                commit('SET_CONDITIONS', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_CONDITIONS', []);
            });
        },

        getMessageOptions({ commit }) {
            return axios.get('/api/getMessageOptions').then(response => {
                commit('SET_MESSAGE_OPTIONS', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_MESSAGE_OPTIONS', []);
            });
        },

        getServiceTypes({ commit }) {
            return axios.get('/api/getServiceTypes').then(response => {
                commit('SET_SERVICE_TYPES', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_SERVICE_TYPES', []);
            });
        },

        getBrands({ commit }) {
            return axios.get('/api/getBrands').then(response => {
                commit('SET_BRANDS', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_BRANDS', []);
            });
        },

        getLanguages({ commit }) {
            return axios.get('/api/getLanguages').then(response => {
                commit('SET_LANGUAGES', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_LANGUAGES', []);
            });
        },

        getGenders({ commit }) {
            return axios.get('/api/getGenders').then(response => {
                commit('SET_GENDERS', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_GENDERS', []);
            });
        },

        getCountryCodes({ commit }) {
            return axios.get('/api/getCountryCodes').then(response => {
                commit('SET_COUNTRY_CODES', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_COUNTRY_CODES', []);
            });
        },

        getDeliveryTypes({ commit }) {
            return axios.get('/api/getDeliveryTypes').then(response => {
                commit('SET_DELIVERY_TYPES', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_DELIVERY_TYPES', []);
            });
        },

        getOrderStatus({ commit }) {
            return axios.get('/api/getOrderStatus').then(response => {
                commit('SET_ORDER_STATUS', response.data);
            }).catch((error) => {
                console.error(error);
                commit('SET_ORDER_STATUS', []);
            });
        },
    }
}
