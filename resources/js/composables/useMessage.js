import { useStore } from 'vuex';

export default function useMessage() {
    const store = useStore();

    function setErrorMessage(messageText) {
        store.dispatch('message/setMessage', {
            message: messageText,
            isVisible: true,
            type: 'error'
        });
    };

    function setNotificationMessage(messageText) {
        store.dispatch('message/setMessage', {
            message: messageText,
            isVisible: true,
            type: 'success'
        });
    };

    return { setErrorMessage, setNotificationMessage };
}