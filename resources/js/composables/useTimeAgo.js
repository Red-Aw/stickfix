import { formatTimeAgo } from '@vueuse/core';

export default function useTimeAgo() {
    function getTimeAgoValue(dateTime) {
        if (!dateTime) {
            return '';
        }

        return formatTimeAgo(Date.parse(dateTime));
    };

    return { getTimeAgoValue };
}