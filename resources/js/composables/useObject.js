export default function useObject() {
    function getEntries(object) {
        return Object.entries(object);
    };

    return { getEntries };
}