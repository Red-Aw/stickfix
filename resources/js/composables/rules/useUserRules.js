import { helpers } from '@vuelidate/validators';
import { ref } from 'vue';
import { $t } from "../../locales/localization";
import useMessage from '../useMessage';
const { withAsync } = helpers;

export default function useUserRules(formValues, isLoading) {
    const emailAvailabilityTimer = ref(null);
    const usernameAvailabilityTimer = ref(null);
    const { setErrorMessage } = useMessage();

    const isEmailAvailableRule = helpers.withMessage($t('validation.emailAlreadyExists'), withAsync(isEmailAvailablePromise));
    const isUsernameAvailableRule = helpers.withMessage($t('validation.usernameAlreadyExists'), withAsync(isUserNameAvailablePromise));

    async function isEmailAvailable(value) {
        const response = await axios.post('/api/checkEmailAvailability', { email: value })
            .then((response) => {
                return response.data.available;
            }).catch((error) => {
                console.error(error);
                formValues.recaptcha = '';
                isLoading.value = true;
                setErrorMessage($t('error.pleaseTryAgainLater'));
                return false;
            });

        return Boolean(await response);
    };

    async function isUserNameAvailable(value) {
        const response = await axios.post('/api/checkUsernameAvailability', { username: value })
            .then((response) => {
                return response.data.available;
            }).catch((error) => {
                console.error(error);
                formValues.recaptcha = '';
                isLoading.value = true;
                setErrorMessage($t('error.pleaseTryAgainLater'));
                return false;
            });

        return Boolean(await response);
    };

    function isEmailAvailablePromise(value) {
        if (isLoading.value) {
            return false;
        }

        if (value === '') {
            return true;
        }

        return new Promise((resolve) => {
            if (emailAvailabilityTimer.value) {
                clearTimeout(emailAvailabilityTimer.value)
                emailAvailabilityTimer.value = null
            }
            emailAvailabilityTimer.value = setTimeout(() => {
                resolve(isEmailAvailable(value));
            }, 500);
        });
    };

    function isUserNameAvailablePromise(value) {
        if (isLoading.value) {
            return false;
        }

        if (value === '') {
            return true;
        }

        return new Promise((resolve) => {
            if (usernameAvailabilityTimer.value) {
                clearTimeout(usernameAvailabilityTimer.value)
                usernameAvailabilityTimer.value = null
            }
            usernameAvailabilityTimer.value = setTimeout(() => {
                resolve(isUserNameAvailable(value));
            }, 500);
        });
    };

    return { isEmailAvailableRule, isUsernameAvailableRule };
}
