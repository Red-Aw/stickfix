import useUrl from './useUrl';

export default function useDefaultImage() {
    const { getPreUrl } = useUrl();

    function getDefaultImage() {
        return getPreUrl() + './uploads/default.png';
    };

    return { getDefaultImage };
}