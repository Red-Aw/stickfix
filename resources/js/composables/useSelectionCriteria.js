import { ref } from 'vue';

export default function useSelectionCriteria() {
    const snowSelectionCriteria = ref(false);
    const selectionCriteriaBtn = ref(
        { title: 'showSelectionCriteria', path: '', icon: 'mdi-chevron-down' }
    );

    function toggleSelectionCriteria() {
        snowSelectionCriteria.value = !snowSelectionCriteria.value;
        if (snowSelectionCriteria.value) {
            selectionCriteriaBtn.value.icon = 'mdi-chevron-up';
            selectionCriteriaBtn.value.title = 'hideSelectionCriteria';
        } else {
            selectionCriteriaBtn.value.icon = 'mdi-chevron-down';
            selectionCriteriaBtn.value.title = 'showSelectionCriteria';
        }
    };

    return { snowSelectionCriteria, selectionCriteriaBtn, toggleSelectionCriteria };
}