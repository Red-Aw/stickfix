export default function useCurrency() {
    function toCurrency(value) {
        if (typeof value !== "number") {
            return value;
        }
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'EUR',
            minimumFractionDigits: 2
        });
        return formatter.format(value / 100);
    };

    function toPrice(number) {
        return parseFloat(number / 100).toFixed(2);
    };

    return { toCurrency, toPrice };
}