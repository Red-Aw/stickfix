import { useRoute } from 'vue-router';

export default function useUrl() {
    function getPreUrl() {
        const route = useRoute();
        let preUrl = '';

        if (route) {
            Object.keys(route.params).forEach(() => {
                preUrl += '../';
            });
        }

        return preUrl;
    };

    function getImageUrl(path, name = '') {
        return getPreUrl() + path + name;
    };

    return { getPreUrl, getImageUrl };
}