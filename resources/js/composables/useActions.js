import { useStore } from 'vuex';

export default function useActions() {
    const store = useStore();

    function updateStore() {
        store.dispatch('items/getItemsList');
        store.dispatch('services/getServicesList');
        store.dispatch('offers/getOffersList');
        store.dispatch('messages/getMessagesList');
        store.dispatch('carousel/getCarouselList');
        store.dispatch('orders/getOrders');
    };

    return { updateStore };
}