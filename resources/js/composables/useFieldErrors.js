import { get, set } from 'lodash';
import { $t } from "../locales/localization";

export default function useFieldErrors(v$, errorValues) {
    function fieldErrors(field) {
        const errors = [];

        if (get(errorValues, field)) {
            errors.push($t(get(errorValues, field)[0]));
        }

        let formValue = get(v$.value, field);
        if (!formValue.$dirty) {
            return errors;
        }

        formValue.$errors.forEach((error) => {
            errors.push(error.$message);
        });

        return errors;
    };

    function resetFieldError(field) {
        if (get(errorValues, field)) {
            set(errorValues, field, null)
        }
    }

    function fieldErrorsWithoutCustomErrors(field) {
        const errors = [];
        if (!v$.value[field].$dirty) {
            return errors;
        }
        v$.value[field].$errors.forEach((error) => {
            errors.push(error.$message);
        });
        return errors;
    };

    return { fieldErrors, resetFieldError, fieldErrorsWithoutCustomErrors };
}
