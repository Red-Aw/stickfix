import { computed } from 'vue';
import { useStore } from 'vuex';

export default function useRole() {
    const store = useStore();

    const userRoles = computed(() => {
        return store.getters['auth/roles'];
    });

    const allRoles = computed(() => {
        return store.getters['auth/allRoles'];
    });

    function isSuperAdmin() {
        if (Array.isArray(userRoles.value)) {
            return userRoles.value.some((record) => record.role === 'superAdmin');
        }
        return false;
    };

    function hasUserRole(role) {
        if (!role) {
            return false;
        }

        if (Array.isArray(userRoles.value)) {
            return userRoles.value.some((record) => record.role === role);
        }
        return false;
    };

    function getVisibleActions(list) {
        if (!list || !list.length) {
            return [];
        }

        if (userRoles.value) {
            return list.filter((item) => isSuperAdmin() || hasUserRole(item.role));
        } else {
            return [];
        }
    };

    function getIsActionVisible(action) {
        if (!action) {
            return false;
        }

        if (userRoles.value) {
            return isSuperAdmin() || hasUserRole(action.role);
        } else {
            return false;
        }
    };

    function getIsActionAllowed(role) {
        if (userRoles.value) {
            return isSuperAdmin() || hasUserRole(role);
        } else {
            return false;
        }
    };

    function getAvailableRoles() {
        if (Array.isArray(allRoles.value) && allRoles.value.length) {
            const availableRoles = allRoles.value;
            return availableRoles.filter((role) => isSuperAdmin() || hasUserRole(role));
        } else {
            return [];
        }
    };

    return { getVisibleActions, getIsActionVisible, getIsActionAllowed, getAvailableRoles };
}