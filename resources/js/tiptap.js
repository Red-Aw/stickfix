import { BaseKit, Blockquote, Bold, BulletList, Clear, Code, CodeBlock, Color, FontFamily, FontSize, Heading, Highlight, History, HorizontalRule, Indent, Italic, Link, OrderedList, Strike, SubAndSuperScript, Table, TaskList, TextAlign, Underline, VuetifyTiptap, VuetifyViewer, createVuetifyProTipTap } from 'vuetify-pro-tiptap';
import 'vuetify-pro-tiptap/styles/editor.css';
import { $t } from "./locales/localization";

export const vuetifyTipTap = createVuetifyProTipTap({
    lang: 'en',
    components: {
        VuetifyTiptap,
        VuetifyViewer
    },
    extensions: [
        BaseKit.configure({
            placeholder: {
                placeholder: $t('description')
            }
        }),
        Bold,
        Italic,
        Underline,
        Strike,
        Code.configure({ divider: true }),
        Heading,
        TextAlign,
        FontFamily,
        FontSize,
        Color,
        Highlight.configure({ divider: true }),
        SubAndSuperScript.configure({ divider: true }),
        Clear.configure({ divider: true }),
        BulletList,
        OrderedList,
        TaskList,
        Indent.configure({ divider: true }),
        Link,
        Table.configure({ divider: true }),
        Blockquote,
        HorizontalRule,
        CodeBlock.configure({ divider: true }),
        History.configure({ divider: true }),
    ]
})