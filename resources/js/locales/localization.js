import { createI18n } from 'vue-i18n/dist/vue-i18n.cjs';

import en from './en.json';
import lv from './lv.json';
import ru from './ru.json';

const i18n = createI18n({
    legacy: false,
    locale: 'lv',
    fallbackLocale: 'lv',
    globalInjection: true,
    messages: {
        en,
        ru,
        lv
    },
});

const $t = i18n.global.t;

export { $t, i18n };
