import axios from 'axios';
import { createRouter, createWebHistory } from 'vue-router';

import Account from './pages/Account.vue';
import AddCarouselItem from './pages/AddCarouselItem.vue';
import AddItem from './pages/AddItem.vue';
import AddOffer from './pages/AddOffer.vue';
import AddService from './pages/AddService.vue';
import AddUser from './pages/AddUser.vue';
import Adverts from './pages/Adverts.vue';
import Cart from './pages/Cart.vue';
import ChangeLanguage from './pages/ChangeLanguage.vue';
import ChangePass from './pages/ChangePass.vue';
import CheckoutSuccess from './pages/CheckoutSuccess.vue';
import ContactsAndRequisites from './pages/ContactsAndRequisites.vue';
import ControlPanel from './pages/ControlPanel.vue';
import Cookies from './pages/Cookies.vue';
import DeliveryTypes from './pages/DeliveryTypes.vue';
import EditCarouselItem from './pages/EditCarouselItem.vue';
import EditItem from './pages/EditItem.vue';
import EditOffer from './pages/EditOffer.vue';
import EditService from './pages/EditService.vue';
import EditShippingData from './pages/EditShippingData.vue';
import EditUser from './pages/EditUser.vue';
import Faq from './pages/Faq.vue';
import GuaranteeAndReturnPolicy from './pages/GuaranteeAndReturnPolicy.vue';
import Home from './pages/Home.vue';
import Item from './pages/Item.vue';
import Login from './pages/Login.vue';
import Message from './pages/Message.vue';
import Messages from './pages/Messages.vue';
import NotFound from './pages/NotFound.vue';
import Order from './pages/Order.vue';
import Orders from './pages/Orders.vue';
import Payment from './pages/Payment.vue';
import PrivacyPolicy from './pages/PrivacyPolicy.vue';
import ProductTables from './pages/ProductTables.vue';
import Shipping from './pages/Shipping.vue';
import Terms from './pages/Terms.vue';
import Users from './pages/Users.vue';
import Vacancies from './pages/Vacancies.vue';
import Verified from './pages/Verified.vue';

export const router = createRouter({
    routes: [
        {
            path: "/:catchAll(.*)*",
            component: NotFound,
            name: 'NotFound',
            meta: { allowFullScreen: true },
        },
        {
            path: '/',
            component: Home,
            name: 'Home'
        },
        {
            path: '/adverts',
            component: Adverts,
            name: 'Adverts',
        },
        {
            path: '/terms',
            component: Terms,
            name: 'Terms'
        },
        {
            path: '/payment',
            component: Payment,
            name: 'Payment'
        },
        {
            path: '/shipping',
            component: Shipping,
            name: 'Shipping'
        },
        {
            path: '/guaranteeAndReturnPolicy',
            component: GuaranteeAndReturnPolicy,
            name: 'GuaranteeAndReturnPolicy'
        },
        {
            path: '/privacyPolicy',
            component: PrivacyPolicy,
            name: 'PrivacyPolicy'
        },
        {
            path: '/cookies',
            component: Cookies,
            name: 'Cookies'
        },
        {
            path: '/contactsAndRequisites',
            component: ContactsAndRequisites,
            name: 'ContactsAndRequisites'
        },
        {
            path: '/faq',
            component: Faq,
            name: 'Faq'
        },
        {
            path: '/vacancies',
            component: Vacancies,
            name: 'Vacancies'
        },
        {
            path: '/login',
            component: Login,
            name: 'Login',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (!response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/addUser',
            component: AddUser,
            name: 'AddUser',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/addNewUser').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/editUser/:id',
            component: EditUser,
            name: 'EditUser',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/editUser').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/newItem',
            component: AddItem,
            name: 'AddItem',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/editItem/:id',
            component: EditItem,
            name: 'EditItem',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/newService',
            component: AddService,
            name: 'AddService',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/editService/:id',
            component: EditService,
            name: 'EditService',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/newOffer',
            component: AddOffer,
            name: 'AddOffer',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/editOffer/:id',
            component: EditOffer,
            name: 'EditOffer',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/account',
            component: Account,
            name: 'Account',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/changePass',
            component: ChangePass,
            name: 'ChangePass',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/changeLanguage',
            component: ChangeLanguage,
            name: 'ChangeLanguage',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/controlPanel',
            component: ControlPanel,
            name: 'ControlPanel',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/enterControlPanel').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/controlPanel/productTables',
            component: ProductTables,
            name: 'ProductTables',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/seeTables').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/controlPanel/users',
            component: Users,
            name: 'Users',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/addNewUser').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/controlPanel/deliveryTypes',
            component: DeliveryTypes,
            name: 'DeliveryTypes',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/seeShippingData').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/messages',
            component: Messages,
            name: 'Messages',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/readMessages').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/message/:id',
            component: Message,
            name: 'Message',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/replyOnMessages').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/newCarouselItem',
            component: AddCarouselItem,
            name: 'AddCarouselItem',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/editCarouselItem/:id',
            component: EditCarouselItem,
            name: 'EditCarouselItem',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/verified/:id',
            component: Verified,
            name: 'Verified',
            beforeEnter: (to, from, next) => {
                const userId = to.params.id;
                axios.get(`/api/checkUser/${userId}`).then(response => {
                    if (!response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/cart',
            component: Cart,
            name: 'Cart',
        },
        {
            path: '/item/:id',
            component: Item,
            name: 'Item',
            beforeEnter: (to, from, next) => {
                const itemId = to.params.id;
                axios.get(`/api/getItem/${itemId}`).then((response) => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch((error) => {
                    return next({ name: 'NotFound'})
                });
            }
        },
        {
            path: '/checkout/success/:sessionId',
            component: CheckoutSuccess,
            name: 'CheckoutSuccess',
            beforeEnter: (to, from, next) => {
                const sessionId = to.params.sessionId;
                axios.get(`/api/getSimpleOrderBySessionId/${sessionId}`).then((response) => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch((error) => {
                    return next({ name: 'NotFound'})
                });
            }
        },
        {
            path: '/orders',
            component: Orders,
            name: 'Orders',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/seeOrders').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/order/:id',
            component: Order,
            name: 'Order',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/seeOrders').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
        {
            path: '/editShippingData/:id',
            component: EditShippingData,
            name: 'EditShippingData',
            beforeEnter: (to, from, next) => {
                axios.get('/api/checkAuth/addOrEditShippingData').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFound'})
                }).catch(() => {
                    return next({ name: 'NotFound'})
                });
            },
        },
    ],
    history: createWebHistory(),
    linkActiveClass: 'active',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }

        return {
            top: 0,
            left: 0,
            behavior: 'smooth'
        };
    },
});