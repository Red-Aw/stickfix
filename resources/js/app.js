import axios from 'axios';
import './bootstrap';
axios.defaults.withCredentials = true;
axios.defaults.baseURL = import.meta.env.VITE_APP_URL;

import { createApp } from 'vue';

import App from './App.vue';
import { i18n } from './locales/localization';
import { router } from './router';
import { store } from "./store";

import { VueReCaptcha } from 'vue-recaptcha-v3';
const vueReCaptchaOptions = {
    siteKey: import.meta.env.VITE_RECAPTCHA_SITE_KEY,
    loaderOptions: {
        useRecaptchaNet: true,
        autoHideBadge: true,
    }
};

import VueCookies from 'vue-cookies';

import '@mdi/font/css/materialdesignicons.css';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import * as labsComponents from 'vuetify/labs/components';
import 'vuetify/styles';

import 'flag-icons/css/flag-icons.min.css';
import { createVPhoneInput } from 'v-phone-input';
import 'v-phone-input/dist/v-phone-input.css';
const vPhoneInput = createVPhoneInput({
    countryIconMode: 'svg',
});

const vuetify = createVuetify({
    components: {
        ...components,
        ...labsComponents,
    },
    directives: {
        ...directives,
    },
    theme: {
        defaultTheme: 'dark'
    }
})

import VuelidatePlugin from '@vuelidate/core';

import { vuetifyTipTap } from './tiptap';

const app = createApp({
    extends: App,
    created() {
        store.dispatch('cart/getCart');
        store.dispatch('items/getItemsList');
        store.dispatch('services/getServicesList');
        store.dispatch('offers/getOffersList');
        store.dispatch('enums/getAllEnums');
        store.dispatch('message/setData');
        store.dispatch('messages/getMessagesList');
        store.dispatch('carousel/getCarouselList');
        store.dispatch('orders/getOrders');
        store.dispatch('shippingData/getShippingData');
        store.dispatch('auth/me');
    },
})
app.use(router);
app.use(vuetify);
app.use(store);
app.use(i18n);
app.use(VueReCaptcha, vueReCaptchaOptions);
app.use(VueCookies);
app.use(VuelidatePlugin);
app.use(vPhoneInput);
app.use(vuetifyTipTap)
app.mount('#app')